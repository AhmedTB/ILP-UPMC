package com.paracamplus.ilp1.ilp1tme1;

import java.io.File;
import java.io.IOException;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.test.InterpreterTest;
import com.paracamplus.ilp1.parser.ParseException;

public class FileInterpreter {

	public static void main(String[] args) throws ParseException, IOException, EvaluationException {
		String fn = args[0];
		File f = new File(fn);
		InterpreterTest i = new InterpreterTest(f);
		i.processFile();
	}

}
