package com.paracamplus.ilp1.ilp1tme1;

import java.io.File;
import java.io.IOException;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.test.CompilerTest;
import com.paracamplus.ilp1.parser.ParseException;

public class FileCompiler {
	
	public static void main(String[] args) throws CompilationException, ParseException, IOException {
		String fn = args[0];
		File f = new File(fn);
		CompilerTest ct = new CompilerTest(f);
		ct.processFile();
	}
}
