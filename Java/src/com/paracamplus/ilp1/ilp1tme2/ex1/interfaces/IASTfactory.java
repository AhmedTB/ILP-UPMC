package com.paracamplus.ilp1.ilp1tme2.ex1.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp1.interfaces.IASTfactory {
	IASTexpression newAffectation(
			IASTvariable variable,
            IASTexpression expression);
	
	IASTexpression newWhile(
            IASTexpression condition,
            IASTexpression consequence);
	
	IASTfunction newFunction(
			IASTvariable variable,
			IASTvariable[] variables,
			IASTexpression body);
}
