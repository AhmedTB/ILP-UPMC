package com.paracamplus.ilp1.ilp1tme2.ex1.interfaces;

public interface IASTvisitor<Result, Data, Anomaly extends Throwable> 
	extends com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly>{
	
	Result visit(IASTaffectation iast, Data data) throws Anomaly;
	Result visit(IASTwhile iast, Data data) throws Anomaly;
}
