package com.paracamplus.ilp1.ilp1tme2.ex1.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTaffectation {
	IASTvariable getVariable(); 
	IASTexpression getExpression();
}
