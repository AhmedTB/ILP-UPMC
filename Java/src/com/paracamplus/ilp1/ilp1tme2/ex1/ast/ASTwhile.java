package com.paracamplus.ilp1.ilp1tme2.ex1.ast;

import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTwhile;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTvisitor;

public class ASTwhile extends ASTexpression implements IASTwhile {

	public ASTwhile(IASTexpression condition,
            IASTexpression consequence) {
		this.condition = condition;
		this.consequence = consequence;
	}
	
	private final IASTexpression condition;
	private final IASTexpression consequence;
	
	@Override
	public IASTexpression getCondition() {
		return condition;
	}
	@Override
	public IASTexpression getConsequence() {
		return consequence;
	}
	@Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor, Data data) throws Anomaly {
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
	
}
