package com.paracamplus.ilp1.ilp1tme2.ex1.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.ilp1tme2.ex1.ast.ASTfunction;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTfunction;

public class ASTfactory extends com.paracamplus.ilp1.ast.ASTfactory 
	implements com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTfactory {

	@Override
	public IASTexpression newAffectation(IASTvariable variable,
			IASTexpression expression) {
		return new ASTaffectation(variable, expression);
	}

	@Override
	public IASTexpression newWhile(IASTexpression condition,
			IASTexpression consequence) {
		return new ASTwhile(condition, consequence);
	}

	@Override
	public IASTfunction newFunction(IASTvariable variable, IASTvariable[] variables, IASTexpression body) {
		return new ASTfunction(variable, variables, body);
	}
	
	
}
