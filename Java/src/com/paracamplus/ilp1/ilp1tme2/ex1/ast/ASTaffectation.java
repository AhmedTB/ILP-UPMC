package com.paracamplus.ilp1.ilp1tme2.ex1.ast;

import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTaffectation;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTvisitor;

public class ASTaffectation extends ASTexpression implements IASTaffectation{

	private final IASTvariable var;
	private final IASTexpression expr;
	
	public ASTaffectation(IASTvariable var, IASTexpression expr) {
		this.var = var;
		this.expr = expr;
	}

	@Override
	public IASTvariable getVariable() {
		return this.var;
	}

	@Override
	public IASTexpression getExpression() {
		return this.expr;
	}

	@Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor, Data data) throws Anomaly {
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}
