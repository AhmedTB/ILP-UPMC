package com.paracamplus.ilp1.ilp1tme2.ex1.ast;

import com.paracamplus.ilp1.ast.ASTnamed;
import com.paracamplus.ilp1.ilp1tme2.ex1.interfaces.IASTfunction;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public class ASTfunction extends ASTnamed implements IASTfunction {
	
	public ASTfunction(IASTvariable variable,
            IASTvariable[] variables,
            IASTexpression body) {
		super(variable.getName());
		this.variable = variable;
		this.variables = variables;
		this.body = body;
	}
	
	private final IASTvariable variable;
	private final IASTvariable [] variables;
	private final IASTexpression body;

	@Override
	public IASTvariable getVariable() {
		return variable;
	}

	@Override
	public IASTvariable[] getVariables() {
		return variables;
	}

	@Override
	public IASTexpression getBody() {
		return body;
	}
	
}