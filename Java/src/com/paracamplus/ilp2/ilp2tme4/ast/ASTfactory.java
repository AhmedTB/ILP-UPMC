package com.paracamplus.ilp2.ilp2tme4.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;



public class ASTfactory extends com.paracamplus.ilp2.ast.ASTfactory
	implements com.paracamplus.ilp2.ilp2tme4.interfaces.IASTfactory {	
	
	@Override
	public IASTexpression newUnless(IASTexpression body,
			IASTexpression condition) {
		return new ASTunless(body, condition);
	}

}
