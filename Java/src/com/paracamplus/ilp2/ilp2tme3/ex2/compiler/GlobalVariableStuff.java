package com.paracamplus.ilp2.ilp2tme3.ex2.compiler;

import com.paracamplus.ilp1.compiler.Primitive;
import com.paracamplus.ilp1.compiler.interfaces.IGlobalVariableEnvironment;


public class GlobalVariableStuff extends com.paracamplus.ilp1.compiler.GlobalVariableStuff {
	
	public GlobalVariableStuff() {
		super();
	}
	
	public static void fillGlobalVariables (IGlobalVariableEnvironment env) {
		com.paracamplus.ilp2.ilp2tme3.ex1.compiler.GlobalVariableStuff.fillGlobalVariables(env);
       env.addGlobalFunctionValue(
               new Primitive("makeVector", "ILP_make_vector", 2));
       env.addGlobalFunctionValue(
    		   new Primitive("vectorLength", "ILP_vector_length", 1));
       env.addGlobalFunctionValue(
    		   new Primitive("vectorGet", "ILP_vector_get", 2));
    }
}
