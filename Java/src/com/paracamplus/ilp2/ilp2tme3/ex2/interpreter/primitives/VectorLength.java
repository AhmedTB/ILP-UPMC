package com.paracamplus.ilp2.ilp2tme3.ex2.interpreter.primitives;

import com.paracamplus.ilp1.interpreter.Interpreter;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.Primitive;

public class VectorLength extends Primitive {

	public VectorLength() {
		super("vectorLength");
	}

	@Override
	public Object apply(Interpreter interpreter, Object[] argument) throws EvaluationException {
		if (argument instanceof Object[]) {
			return argument.length;
		} else {
            String msg = "arg1 must be an array";
            throw new EvaluationException(msg);
        }
	}

	@Override
	public int getArity() {
		return 1;
	}

	
}
