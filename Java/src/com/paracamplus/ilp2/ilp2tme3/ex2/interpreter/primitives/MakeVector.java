package com.paracamplus.ilp2.ilp2tme3.ex2.interpreter.primitives;

import java.math.BigDecimal;
import java.math.BigInteger;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.Primitive;

public class MakeVector extends Primitive {
	public MakeVector() {
		super("makeVector");
	}
	
   
	@Override
	public Object apply(Object arg1, Object arg2) throws EvaluationException {
        if ( arg1 instanceof BigInteger ) {
            BigInteger bi1 = (BigInteger) arg1;
            Object [] vector = new Object[bi1.intValue()];
            for (Object i: vector) {
            	i = arg2;
            }
            return vector;
        } else {
            String msg = "arg1 must be a BigInteger";
            throw new EvaluationException(msg);
        }
    }


	@Override
	public int getArity() {
		return 2;
	}

	
}
