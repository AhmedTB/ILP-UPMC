package com.paracamplus.ilp2.ilp2tme3.ex1.interpreter.primitive;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class Sinus extends UnaryPrimitive {
	
	public Sinus() {
		super("sinus");
	}
   
	@Override
	public Object apply(Object arg1) throws EvaluationException {
        if ( arg1 instanceof BigInteger ) {
            BigInteger bi1 = (BigInteger) arg1;
            return new BigDecimal(Math.sin(bi1.doubleValue()));
        } else if ( arg1 instanceof BigDecimal ) {
            BigDecimal bd1 = (BigDecimal) arg1;
            return new BigDecimal(Math.sin(bd1.doubleValue()));
        } else {
            String msg = "arg1 must be a decimal";
            throw new EvaluationException(msg);
        }
    }
}