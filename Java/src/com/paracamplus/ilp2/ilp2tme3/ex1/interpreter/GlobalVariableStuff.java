package com.paracamplus.ilp2.ilp2tme3.ex1.interpreter;

import java.io.Writer;

import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp2.ilp2tme3.ex1.interpreter.primitive.Sinus;

public class GlobalVariableStuff extends com.paracamplus.ilp1.interpreter.GlobalVariableEnvironment {
	
	public GlobalVariableStuff() {
		super();
	}
	
	public static void fillGlobalVariables (
            IGlobalVariableEnvironment env,
            Writer out) {
		com.paracamplus.ilp1.interpreter.GlobalVariableStuff.fillGlobalVariables(env, out);
		env.addGlobalVariableValue(new Sinus());
    }
}
