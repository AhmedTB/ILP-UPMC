package com.paracamplus.ilp2.partial_16.parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.paracamplus.ilp1.parser.ParseException;
import com.paracamplus.ilp2.interfaces.IASTprogram;

import com.paracamplus.ilp2.partial_16.interfaces.IASTfactory;
import com.paracamplus.ilp2.partial_16.parser.ILPMLListener;
import antlr4.ILPMLgrammar_partial_2016Parser;
import antlr4.ILPMLgrammar_partial_2016Lexer;


public class ILPMLParser extends com.paracamplus.ilp2.parser.ilpml.ILPMLParser{
	public ILPMLParser(IASTfactory factory) {
		super(factory);
	}

	public IASTprogram getProgram() throws ParseException {
		try {
			ANTLRInputStream in = new ANTLRInputStream(input.getText());
			// flux de caractères -> analyseur lexical
			ILPMLgrammar_partial_2016Lexer lexer = new ILPMLgrammar_partial_2016Lexer(in);
			// analyseur lexical -> flux de tokens
			CommonTokenStream tokens =	new CommonTokenStream(lexer);
			// flux tokens -> analyseur syntaxique
			ILPMLgrammar_partial_2016Parser parser =	new ILPMLgrammar_partial_2016Parser(tokens);
			// démarage de l'analyse syntaxique
			ILPMLgrammar_partial_2016Parser.ProgContext tree = parser.prog();		
			// parcours de l'arbre syntaxique et appels du Listener
			ParseTreeWalker walker = new ParseTreeWalker();
			ILPMLListener extractor = new ILPMLListener((IASTfactory)factory);
			walker.walk(extractor, tree);	
			return tree.node;
			} catch (Exception e) {
				throw new ParseException(e);
			}
	}

}
