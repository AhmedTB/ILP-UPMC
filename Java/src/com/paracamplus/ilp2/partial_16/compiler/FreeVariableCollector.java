package com.paracamplus.ilp2.partial_16.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp2.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp2.partial_16.interfaces.IASTvisitor;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;

public class FreeVariableCollector extends com.paracamplus.ilp2.compiler.FreeVariableCollector
implements IASTvisitor<Void, Set<IASTClocalVariable>, CompilationException> {

    public FreeVariableCollector(IASTCprogram program) {
        super(program);
    }

	@Override
	public Void visit(IASTfunctionDefinition iast, Set<IASTClocalVariable> data)
		throws CompilationException {
		iast.getFunctionVariable().accept(this, data);
		iast.getVariables().accept(this, data);
		iast.getBindings().accept(this, data);
		iast.getBody().accept(this, data);
		return null;
	}

	@Override
	public Void visit(IASTinvocation iast, Set<IASTClocalVariable> data)
		throws CompilationException {
		iast.getFunction().accept(this, data);
		iast.getArguments().accept(this, data);
		return null;
	}
}
