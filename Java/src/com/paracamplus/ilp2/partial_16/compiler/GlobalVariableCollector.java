package com.paracamplus.ilp2.partial_16.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp2.partial_16.interfaces.IASTvisitor;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;

public class GlobalVariableCollector extends com.paracamplus.ilp2.compiler.GlobalVariableCollector
implements IASTvisitor<Set<IASTCglobalVariable>, 
                        Set<IASTCglobalVariable>, 
                        CompilationException> {

	@Override
	public Set<IASTCglobalVariable> visit(IASTfunctionDefinition iast, Set<IASTCglobalVariable> data)
		throws CompilationException {
		result = iast.getFunctionVariable().accept(this, data);
		result = iast.getVariables().accept(this, data);
		result = iast.getBindings().accept(this, data);
		result = iast.getBody().accept(this, data);
		return result;
	}

	@Override
	public Set<IASTCglobalVariable> visit(IASTinvocation iast, Set<IASTCglobalVariable> data)
		throws CompilationException {
		result = iast.getFunction().accept(this, data);
		result = iast.getArguments().accept(this, data);
		return result;
	}
}
