package com.paracamplus.ilp2.partial_16.compiler.normalizer;


import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;
import com.paracamplus.ilp2.partial_16.interfaces.IASTvisitor;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;


public class Normalizer 
extends com.paracamplus.ilp2.compiler.normalizer.Normalizer 
implements 
    IASTvisitor<IASTexpression, INormalizationEnvironment, CompilationException> {

    public Normalizer (INormalizationFactory factory) {
    	super(factory);
    }

	@Override
	public IASTexpression visit(IASTfunctionDefinition iast, INormalizationEnvironment data)
		throws CompilationException {
		IASTexpression functionVariable = iast.getFunctionVariable().accept(this, data);
		IASTexpression[] variables = new IASTvariable[iast.getVariables().length];
	
		for (int i=0; i< iast.getVariables().length; i++) {
			variables[i] = iast.getVariables()[i].accept(this, data);
		}
		
		IASTbinding[] bindings = new IASTbinding[iast.getBindings().length];
		
		for (int i=0; i< iast.getBindings().length; i++) {
			bindings[i] = iast.getBindings()[i];
		}
		
		IASTexpression body = iast.getBody().accept(this, data);
		return ((INormalizationFactory) factory).newFunctionDefinition(functionVariable, ((IASTvariable[]) variables), bindings, body);
	}

	@Override
	public IASTexpression visit(IASTinvocation iast, INormalizationEnvironment data)
		throws CompilationException {
		IASTexpression function = iast.getFunction().accept(this, data);
		IASTexpression[] arguments = new IASTexpression[iast.getArguments().length];
		
		for (int i=0; i<iast.getArguments().length; i++) {
			arguments[i] = iast.getArguments()[i].accept(this, data);
		}
		
		IASTbinding[] binds = new IASTbinding[iast.getBinds().length];
		
		for (int i=0; i<iast.getBinds().length; i++) {
			binds[i] = iast.getBinds()[i];
		}
		
		return ((INormalizationFactory) factory).newInvocation(function, arguments, binds);
	}
}
