package com.paracamplus.ilp2.partial_16.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;

public interface INormalizationFactory 
 	extends com.paracamplus.ilp2.compiler.normalizer.INormalizationFactory {
	IASTfunctionDefinition newFunctionDefinition(IASTvariable functionVariable, IASTvariable[] variables, IASTbinding[] bindings, IASTexpression body);
	IASTinvocation newInvocation(IASTexpression function, IASTexpression[] arguments, IASTbinding[] binds);
}
