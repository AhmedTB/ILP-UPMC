package com.paracamplus.ilp2.partial_16.interfaces;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp2.interfaces.IASTfactory {
	IASTfunctionDefinition newFunctionDefinition(IASTvariable functionVariable,
			IASTvariable[] variables, IASTbinding[] bindings, IASTexpression body);
	IASTinvocation newInvocation(IASTexpression function, IASTexpression[] arguments,
			IASTbinding[] binds);
}
