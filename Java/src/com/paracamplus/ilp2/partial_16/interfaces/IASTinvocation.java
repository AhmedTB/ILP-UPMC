package com.paracamplus.ilp2.partial_16.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;

public interface IASTinvocation extends IASTexpression {
    
    IASTexpression getFunction();
	IASTexpression[] getArguments();
	IASTbinding[] getBinds();
	
}
