package com.paracamplus.ilp2.partial_16.interfaces;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfunctionDefinition extends com.paracamplus.ilp2.interfaces.IASTfunctionDefinition {
    
	IASTvariable[] getVariables();
	IASTbinding[] getBindings();
	
}
