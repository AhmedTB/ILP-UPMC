package com.paracamplus.ilp2.partial_16.ast;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;
import com.paracamplus.ilp2.partial_16.interfaces.IASTvisitor;
import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;

public class ASTinvocation extends ASTexpression 
implements IASTinvocation {

    public ASTinvocation(IASTexpression function, IASTexpression[] arguments, IASTbinding[] binds) {
        this.function = function;
		this.arguments = arguments;
		this.binds = binds;
		
    }
    
    private final IASTexpression function;
	private final IASTexpression[] arguments;
	private final IASTbinding[] binds;
	
    @Override
	public IASTexpression getFunction(){
		return function;
	}
	@Override
	public IASTexpression[] getArguments(){
		return arguments;
	}
	@Override
	public IASTbinding[] getBinds() {
		return binds;
	}
	
    @Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor,
			Data data) throws Anomaly {
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}
