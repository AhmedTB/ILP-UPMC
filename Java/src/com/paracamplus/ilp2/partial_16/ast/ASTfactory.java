package com.paracamplus.ilp2.partial_16.ast;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;

public class ASTfactory extends com.paracamplus.ilp2.ast.ASTfactory
	implements com.paracamplus.ilp2.partial_16.interfaces.IASTfactory {	

	@Override
	public IASTfunctionDefinition newFunctionDefinition(IASTvariable functionVariable, IASTvariable[] variables,
			IASTbinding[] bindings, IASTexpression body) {
		return new ASTfunctionDefinition(functionVariable, variables, bindings, body);
	}

	@Override
	public IASTinvocation newInvocation(IASTexpression function, IASTexpression[] arguments,
			IASTbinding[] binds) {
		return new ASTinvocation(function, arguments, binds);
	}
}
