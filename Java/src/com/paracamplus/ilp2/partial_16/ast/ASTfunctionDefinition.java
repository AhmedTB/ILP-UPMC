package com.paracamplus.ilp2.partial_16.ast;


import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp1.ast.ASTnamed;

public class ASTfunctionDefinition extends ASTnamed 
implements IASTfunctionDefinition {

    public ASTfunctionDefinition(IASTvariable functionVariable, IASTvariable[] variables,
    		IASTbinding[] bindings, IASTexpression body) {
    	super(functionVariable.getName());
        this.functionVariable = functionVariable;
		this.variables = variables;
		this.bindings = bindings;
		this.body = body;
		
    }
    
    private final IASTvariable functionVariable;
	private final IASTvariable[] variables;
	private final IASTbinding[] bindings;
	private final IASTexpression body;
	
    @Override
	public IASTvariable getFunctionVariable(){
		return functionVariable;
	}
    
	@Override
	public IASTvariable[] getVariables(){
		return variables;
	}
	
	@Override
	public IASTbinding[] getBindings(){
		return bindings;
	}
	
	@Override
	public IASTexpression getBody(){
		return body;
	}
	
}
