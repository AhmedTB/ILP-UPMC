package com.paracamplus.ilp2.partial_16.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;

import com.paracamplus.ilp2.partial_16.interfaces.IASTvisitor;
import com.paracamplus.ilp2.partial_16.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.partial_16.interfaces.IASTinvocation;

public class Interpreter extends com.paracamplus.ilp2.interpreter.Interpreter
	implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException>{

	public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
            IOperatorEnvironment operatorEnvironment ) {
		super(globalVariableEnvironment, operatorEnvironment);
	}
	
	private static Object whatever = "whatever";

	@Override
	public Object visit(IASTfunctionDefinition iast, ILexicalEnvironment data)
		throws EvaluationException {
		return null;
	}

	@Override
	public Object visit(IASTinvocation iast, ILexicalEnvironment data)
		throws EvaluationException {
		return null;
	}

}
