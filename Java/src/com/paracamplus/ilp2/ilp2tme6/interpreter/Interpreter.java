package com.paracamplus.ilp2.ilp2tme6.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp2.interfaces.IASTvisitor;


public class Interpreter extends com.paracamplus.ilp2.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException>{

	public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
			IOperatorEnvironment operatorEnvironment ) {
		super(globalVariableEnvironment, operatorEnvironment);
	}

}
