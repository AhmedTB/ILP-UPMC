package com.paracamplus.ilp2.ilp2tme6;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp1.compiler.normalizer.NoSuchLocalVariableException;
import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp2.ast.ASTfactory;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;

public class RenameTransform extends CopyTransform<INormalizationEnvironment> {

	private int cptglobal;
	
	public RenameTransform(ASTfactory factory) {
		super(factory);
		cptglobal = 0;
	}
	
	@Override
	public IASTexpression visit(IASTvariable iast, INormalizationEnvironment data)
			throws NoSuchLocalVariableException {
		try {
			return data.renaming(iast);
		} catch(NoSuchLocalVariableException nslve) {
			return factory.newVariable(iast.getName());
		}
	}
	
	
	@Override
	public IASTexpression visit(IASTblock iast, INormalizationEnvironment data)
			throws CompilationException {
		
		IASTbinding[] oldbinding = iast.getBindings();
		IASTbinding[] binding =new IASTbinding[oldbinding.length];
		
		INormalizationEnvironment data2 = data;
	
		for (int i = 0; i < oldbinding.length; i++) {
			IASTvariable v = (IASTvariable) oldbinding[i].getVariable();
			String rname = v.getMangledName()+"_"+cptglobal;
			IASTvariable nv = factory.newVariable(rname);
			data2 = data2.extend(v, nv);
			
			IASTexpression exp = oldbinding[i].getInitialisation().accept(this, data);
			binding[i] = factory.newBinding(nv, exp);
			cptglobal++;
		}
		
		IASTexpression body = iast.getBody().accept(this, data2);
		return factory.newBlock(binding, body);
	}
	
	@Override
	public IASTfunctionDefinition visit(IASTfunctionDefinition iast, INormalizationEnvironment data) 
			throws CompilationException {
		
		
		IASTvariable functionVariable  = (IASTvariable) iast.getFunctionVariable().accept(this,data);
		
		IASTvariable[] oldvariables = iast.getVariables();
		
		IASTvariable[] variables = new IASTvariable[oldvariables.length];
		for (int i = 0; i < oldvariables.length; i++) {
			String rnamev = oldvariables[i].getMangledName()+"_"+cptglobal;
			IASTvariable nv = factory.newVariable(rnamev);
			variables[i] = nv;
			data = data.extend(oldvariables[i], nv);
			cptglobal++;
		}
		
		IASTexpression body = iast.getBody().accept(this, data);
		return factory.newFunctionDefinition(functionVariable, variables, body);
	}
	

}
