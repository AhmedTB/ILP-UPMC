package com.paracamplus.ilp2.ilp2tme6;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.interfaces.IASTalternative;
import com.paracamplus.ilp1.interfaces.IASTbinaryOperation;
import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTboolean;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTfloat;
import com.paracamplus.ilp1.interfaces.IASTinteger;
import com.paracamplus.ilp1.interfaces.IASTinvocation;
import com.paracamplus.ilp1.interfaces.IASTsequence;
import com.paracamplus.ilp1.interfaces.IASTstring;
import com.paracamplus.ilp1.interfaces.IASTunaryOperation;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.interfaces.IASTassignment;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp2.interfaces.IASTloop;
import com.paracamplus.ilp2.interfaces.IASTprogram;
import com.paracamplus.ilp2.interfaces.IASTvisitor;

public class CallAnalysis implements IASTvisitor<Void, Set<String>, CompilationException>{

	@Override
	public Void visit(IASTalternative iast, Set<String> data) throws CompilationException {
		iast.getCondition().accept(this, data);
		iast.getConsequence().accept(this, data);
		if (iast.getAlternant() != null) {
			iast.getAlternant().accept(this, data);
		}
		
		return null;
	}

	@Override
	public Void visit(IASTbinaryOperation iast, Set<String> data)
			throws CompilationException {
		iast.getLeftOperand().accept(this, data);
		iast.getRightOperand().accept(this, data);
		
		return null;
	}

	@Override
	public Void visit(IASTblock iast, Set<String> data) throws CompilationException {
		IASTbinding[] bs = iast.getBindings();
		for (IASTbinding b: bs) {
			b.getInitialisation().accept(this, data);
		}
		iast.getBody().accept(this, data);
		
		return null;
	}

	@Override
	public Void visit(IASTboolean iast, Set<String> data) throws CompilationException {
		return null;
	}

	@Override
	public Void visit(IASTfloat iast, Set<String> data) throws CompilationException {
		return null;
	}

	@Override
	public Void visit(IASTinteger iast, Set<String> data) throws CompilationException {
		return null;
	}

	@Override
	public Void visit(IASTinvocation iast, Set<String> data) throws CompilationException {
		IASTexpression[] exprs = iast.getArguments();
		for(IASTexpression e: exprs) {
			e.accept(this, data);
		}
		
		/* Limitation, on suppose que l'expression dénotant la fonction appelée
		 * est réduite à une variable, qui dénote une fonction globale...
		 */
		IASTexpression function = iast.getFunction();
		if (iast.getFunction() instanceof IASTvariable) {
			data.add(((IASTvariable)function).getName());
		} else {
			throw new CompilationException("unsupported function call");
		}
		
		return null;
	}

	@Override
	public Void visit(IASTsequence iast, Set<String> data) throws CompilationException {
		IASTexpression[] exprs = iast.getExpressions();
		for (IASTexpression e : exprs) {
			e.accept(this, data);
		}
		
		return null;
	}

	@Override
	public Void visit(IASTstring iast, Set<String> data) throws CompilationException {
		return null;
	}

	@Override
	public Void visit(IASTunaryOperation iast, Set<String> data)
			throws CompilationException {
		iast.getOperand().accept(this, data);
		return null;
	}

	@Override
	public Void visit(IASTvariable iast, Set<String> data) throws CompilationException {
		iast.accept(this, data);
		return null;
	}

	@Override
	public Void visit(IASTassignment iast, Set<String> data) throws CompilationException {
		iast.getExpression().accept(this, data);
		return null;
	}

	@Override
	public Void visit(IASTloop iast, Set<String> data) throws CompilationException {
		while(true) {
			iast.getCondition().accept(this, data);
			iast.getBody().accept(this, data);
		}
	}
	
	public Void visit(IASTfunctionDefinition iast, Set<String> data) 
		throws CompilationException {
		//Visite du corps sur un ensemble frais
		Set<String> set = new HashSet<>();
		iast.getBody().accept(this, set);
		
		// Supression des variables formels
		for (IASTvariable v : iast.getVariables()) {
			set.remove(v.getName());
		}
		
		//Ajout du contenu dans l'ensemble
		data.addAll(set);
		return null;
	}
	
	protected Set<String> recursive = null;
	
	public boolean isRecursive(IASTvariable var) {
		return recursive.contains(var.getName());
	}
	
	public Void visit(IASTprogram p) throws CompilationException {
		Map<String, Set<String>> calls = new HashMap<>();
		
		for (IASTfunctionDefinition fd : p.getFunctionDefinitions()) {
			Set<String> set = new HashSet<>();
			visit(fd, set);
			calls.put(fd.getFunctionVariable().getName(), set);
		}
		
		recursive = new HashSet<>();
		
		for (String v : calls.keySet()) {
			Set<String> reachable = new HashSet<>();
			Stack<String> todo = new Stack<>();
			todo.push(v);
			
			while(!todo.empty()) {
				String cur = todo.pop();
				if(reachable.contains(cur)) {
					continue;
				}
				reachable.add(cur);
				Set<String> nexts = calls.get(cur);
				if(nexts != null) {
					for (String next : nexts) {
						if (v.equals(next)) {
							recursive.add(v);
						}
						todo.push(next);
					}
				}
			}
		}
		return null;
		
	}
}

	