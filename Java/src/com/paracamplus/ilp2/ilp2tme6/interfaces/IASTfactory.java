package com.paracamplus.ilp2.ilp2tme6.interfaces;

import com.paracamplus.ilp2.ilp2tme6.RenameTransform;

public interface IASTfactory extends com.paracamplus.ilp2.interfaces.IASTfactory {
	RenameTransform newRenameTransform();
}
