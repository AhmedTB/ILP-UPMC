package com.paracamplus.ilp2.ilp2tme6.ast;

import com.paracamplus.ilp2.ilp2tme6.RenameTransform;

public class ASTfactory extends com.paracamplus.ilp2.ast.ASTfactory
	implements com.paracamplus.ilp2.ilp2tme6.interfaces.IASTfactory {

	@Override
	public RenameTransform newRenameTransform() {
		return new RenameTransform(this);
	}

	
}
