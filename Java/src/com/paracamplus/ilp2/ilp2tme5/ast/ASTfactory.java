package com.paracamplus.ilp2.ilp2tme5.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;



public class ASTfactory extends com.paracamplus.ilp2.ast.ASTfactory
	implements com.paracamplus.ilp2.ilp2tme5.interfaces.IASTfactory {

	@Override
	public IASTexpression newBreak(String label) {
		return new ASTbreak(label);
	}

	@Override
	public IASTexpression newContinue(String label) {
		return new ASTcontinue(label);
	}
	
	@Override
	public IASTexpression newLoop(IASTexpression condition, IASTexpression body, String label) {
		return new ASTloop(condition, body, label);
	}
}
