package com.paracamplus.ilp2.ilp2tme5.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp2.ilp2tme5.interfaces.IASTloop;

public class ASTloop extends com.paracamplus.ilp2.ast.ASTloop implements IASTloop {

	private String label;
	
	public ASTloop(IASTexpression condition, IASTexpression body,
				String label) {
		super(condition, body);
		this.label = label;
	}
	
	@Override
	public String getLabel() {
		return label;
	}

}
