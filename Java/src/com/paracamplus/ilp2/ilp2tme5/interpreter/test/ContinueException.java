package com.paracamplus.ilp2.ilp2tme5.interpreter.test;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class ContinueException extends EvaluationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String label;
	
	public ContinueException(String label) {
		super(label);
	}
	
	public String getLabel() {
		return label;
	}
}
