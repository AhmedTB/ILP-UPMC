package com.paracamplus.ilp2.ilp2tme5.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp2.ilp2tme5.interfaces.IASTbreak;
import com.paracamplus.ilp2.ilp2tme5.interfaces.IASTcontinue;
import com.paracamplus.ilp2.ilp2tme5.interfaces.IASTvisitor;
import com.paracamplus.ilp2.ilp2tme5.interpreter.test.BreakException;
import com.paracamplus.ilp2.ilp2tme5.interpreter.test.ContinueException;
import com.paracamplus.ilp2.ilp2tme5.interfaces.IASTloop;

public class Interpreter extends com.paracamplus.ilp2.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException>{

	public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
			IOperatorEnvironment operatorEnvironment ) {
		super(globalVariableEnvironment, operatorEnvironment);
	}

	@Override
	public Object visit(IASTloop iast, ILexicalEnvironment lexenv) 
			throws EvaluationException {
		while ( true ) {
			Object condition = iast.getCondition().accept(this, lexenv);
			if ( condition instanceof Boolean ) {
				Boolean c = (Boolean) condition;
				if ( ! c ) {
					break;
				}
			}
			try {
				iast.getBody().accept(this, lexenv);
			} catch (EvaluationException e) {
				if ((e.getCause() != null) && (e instanceof BreakException) &&
						(((BreakException)e.getCause()).getLabel() == null ||
						iast.getLabel() == ((BreakException)e.getCause()).getLabel())) {
					break;
				}
				if ((e.getCause() != null) && (e instanceof ContinueException) &&
						(((ContinueException)e.getCause()).getLabel() == null || 
						iast.getLabel() == ((ContinueException)e.getCause()).getLabel())) {
					continue;
				}
				
				throw e;
			}
		}
        return Boolean.FALSE;
	}


		@Override
		public Object visit(IASTbreak iast, ILexicalEnvironment data)
				throws EvaluationException {
			throw new BreakException(iast.getLabel());
		}

		@Override
		public Object visit(IASTcontinue iast, ILexicalEnvironment data)
				throws EvaluationException {
			throw new ContinueException(iast.getLabel());	
		}


	}
