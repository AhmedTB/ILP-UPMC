/* *****************************************************************
 * ilp3 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp3
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp3.ilp3tme6.interpreter;

import java.util.List;
import java.util.Vector;

import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp3.ilp3tme6.interfaces.IASTcostart;
import com.paracamplus.ilp3.ilp3tme6.interfaces.IASTvisitor;
import com.paracamplus.ilp3.interfaces.IASTprogram;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.Invocable;
import com.paracamplus.ilp3.interpreter.primitive.Throw.ThrownException;

public class Interpreter extends com.paracamplus.ilp3.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException> {
    
	 public Interpreter(IGlobalVariableEnvironment globalVariableEnvironment,
				IOperatorEnvironment operatorEnvironment) {
			super(globalVariableEnvironment, operatorEnvironment);
		}


    @Override 
    public Object visit(com.paracamplus.ilp1.interfaces.IASTprogram iast, ILexicalEnvironment lexenv) throws EvaluationException {
    	return visit((IASTprogram)iast, lexenv);
    }

    public Object visit(IASTprogram iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {
        for ( IASTfunctionDefinition fd : iast.getFunctionDefinitions() ) {
            Object f = this.visit(fd, lexenv);
            String v = fd.getName();
            getGlobalVariableEnvironment().addGlobalVariableValue(v, f);
        }
        try {
            return iast.getBody().accept(this, lexenv);
        } catch (ThrownException exc) {
            return exc.getThrownValue();
        } catch (Exception exc) {
            return exc;
        }
    }

    // 

	@Override
	public Object visit(IASTcostart iast, ILexicalEnvironment data)
			throws EvaluationException {
		
		Object function = iast.getFunction().accept(this, data);
		
		if (function instanceof Invocable){
			 List<Object> args = new Vector<Object>();
			 for ( IASTexpression arg : iast.getArguments() ) {
	        	 Object value = arg.accept(this, data);
	        	 args.add(value);
	        	 
	         }
			 return new CoroutineInstance(this, args.toArray());
		} else {
			String msg = "Cannot apply " + function;
            throw new EvaluationException(msg);
		}
	}
   


}
