package com.paracamplus.ilp3.ilp3tme6.interpreter.primitive;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.Primitive;
import com.paracamplus.ilp3.ilp3tme6.interpreter.CoroutineInstance;

public class Yield extends Primitive {

    public Yield () {
        super("yield");
    }
    
    @Override
	public int getArity () {
        return 0;
    }
    
    public Object apply() throws EvaluationException {
    	CoroutineInstance c = CoroutineInstance.currentCoroutine();
    	if (c == null) {
    		throw new EvaluationException("yield outside a coroutine");
    	}
        c.yieldCoroutine();
        return Boolean.FALSE;
    }
   
}
