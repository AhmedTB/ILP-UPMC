package com.paracamplus.ilp3.ilp3tme6.interpreter.primitive;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;
import com.paracamplus.ilp3.ilp3tme6.interpreter.CoroutineInstance;

public class Resume extends UnaryPrimitive {

    public Resume () {
        super("resume");
    }
    
    @Override
	public Object apply (Object value) throws EvaluationException {
    	if (!(value instanceof CoroutineInstance)) 
    		throw new EvaluationException("argument is not a coroutine");
    	
    	CoroutineInstance c = (CoroutineInstance) value;
    	c.resumeCoroutine();
    	return Boolean.FALSE;
    }
}
