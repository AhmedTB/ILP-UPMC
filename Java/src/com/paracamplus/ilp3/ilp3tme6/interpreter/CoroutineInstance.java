package com.paracamplus.ilp3.ilp3tme6.interpreter;

import java.util.concurrent.Semaphore;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.Invocable;

public class CoroutineInstance extends Thread {
	
	Semaphore resumeSemaphore = new Semaphore(0);
	Semaphore yieldSemaphore = new Semaphore(0);
	Interpreter i;
	Object[] args;
	Invocable f;
	boolean finished;
	
	public CoroutineInstance(Interpreter i, Object[] args) {
		this.i = i;
		this.args = args;
		finished = false;
		start();
	}
	
	public void run() {
		try {
			// Attente du premier resume
			resumeSemaphore.acquireUninterruptibly();
			// Exécution du point d'entrée
			f.apply(i, args);
			
		} catch (EvaluationException e) {
			e.printStackTrace();
		}
		
		finished = true;
	}
	
	 public boolean resumeCoroutine() {
		 if (finished) return false;
		 
		 resumeSemaphore.release();
		 // Maintenant j'attends un yield
		 yieldSemaphore.acquireUninterruptibly();
		 return true;
	 }
	 
	 public void yieldCoroutine() {
		 yieldSemaphore.release();
		 // J'attends un semaphore
		 resumeSemaphore.acquireUninterruptibly();
	 }
	 
	 public static CoroutineInstance currentCoroutine() {
		 Thread t = Thread.currentThread();
		 if (t instanceof CoroutineInstance) {
			 return (CoroutineInstance) t;
		 }
		 return null;
	 }
	
}
