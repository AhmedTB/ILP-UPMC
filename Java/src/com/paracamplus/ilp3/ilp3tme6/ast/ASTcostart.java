/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp3.ilp3tme6.ast;

import com.paracamplus.ilp1.ast.ASTinvocation;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp3.ilp3tme6.interfaces.IASTcostart;
import com.paracamplus.ilp3.ilp3tme6.interfaces.IASTvisitor;

public class ASTcostart extends ASTinvocation implements IASTcostart {
    
    public ASTcostart (IASTexpression function, IASTexpression[] arguments) {
        super(function, arguments);
    }
 
    @Override
	public <Result, Data, Anomaly extends Throwable> 
    Result accept(com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor, Data data)
            throws Anomaly {
    	return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
    }
}
