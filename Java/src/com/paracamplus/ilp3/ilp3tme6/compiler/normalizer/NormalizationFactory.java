/* *****************************************************************
 * ilp2 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp2
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp3.ilp3tme6.compiler.normalizer;

import com.paracamplus.ilp3.ilp3tme6.ast.ASTcostart;
import com.paracamplus.ilp1.interfaces.IASTexpression;

public class NormalizationFactory
extends com.paracamplus.ilp3.compiler.normalizer.NormalizationFactory
implements INormalizationFactory {
    
	@Override
	public IASTexpression newCostart(IASTexpression function,
			IASTexpression[] arguments) {
		return new ASTcostart(function, arguments);
	}


}
