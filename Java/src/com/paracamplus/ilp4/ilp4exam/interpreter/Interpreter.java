package com.paracamplus.ilp4.ilp4exam.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp4.interpreter.interfaces.IClassEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;

import com.paracamplus.ilp4.ilp4exam.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTSuper_with_args;

public class Interpreter extends com.paracamplus.ilp4.interpreter.Interpreter
	implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException>{

public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
            IOperatorEnvironment operatorEnvironment,
            IClassEnvironment classEnvironment ) {
		super(globalVariableEnvironment, operatorEnvironment, classEnvironment);
		this.classEnvironment = classEnvironment;
	}

    protected IClassEnvironment classEnvironment;
    
    public IClassEnvironment getClassEnvironment () {
        return classEnvironment;
    }


	@Override
	public Object visit(IASTSuper_with_args iast, ILexicalEnvironment data)
		throws EvaluationException {
		return null;
	}



/*
 * Exemple Objet : 
 * 
 * 	@Override
	public Object visit(IASTreadProperty iast, ILexicalEnvironment data)
			throws EvaluationException {
		
		Object nom = iast.getNom().accept(this, data);
		Object property = iast.getProperty().accept(this, data);

		// Le nom est objet de type ILP9Instance 
		if(nom instanceof ILP9Instance){
			ILP9Instance instance =(ILP9Instance)nom;
			
			// property est la propriete qui est une String 
			if(property instanceof String){
				String prop=(String)property;
			
				// Si la propriete existe on renvoie sa valeur sinon ca leve une exception
				return instance.readProperty(prop);
			}
			throw new ExceptionProperty(property.toString() + " n'est pas une String !");
		}
		
		throw new ExceptionProperty(nom.toString() + " n'est pas un ILP9Instance !");
	}
 * 
 *  */
	
	
	
	
	/*
	 * IMPLEMENTER CETTE METHODE EN FONCTION DE L'AST CRÉÉ
	 * 
	 * 	@Override
		public Object visit(IASTloop iast, ILexicalEnvironment lexenv)
		 throws EvaluationException {
	        while ( true ) {
	            Object condition = iast.getCondition().accept(this, lexenv);
	            if ( condition instanceof Boolean ) {
	                Boolean c = (Boolean) condition;
	                if ( ! c ) {
	                    break;
	                }
	            }
	            iast.getBody().accept(this, lexenv);
	        }
	        return Boolean.FALSE;
    	}
	 * 
	 */
	
	
	
	/**
	 * si type break ou continue 	
	 * 
	 * @Override
		public Object visit(IASTbreak iast, ILexicalEnvironment data) 
			throws EvaluationException {
			String label = iast.getLabel();
			throw new BreakException(label);
		}
	 * 
	 * 
	 */



}
