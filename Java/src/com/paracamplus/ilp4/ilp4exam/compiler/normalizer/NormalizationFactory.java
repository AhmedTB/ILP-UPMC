package com.paracamplus.ilp4.ilp4exam.compiler.normalizer;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4exam.ast.ASTSuper_with_args;

public class NormalizationFactory
extends com.paracamplus.ilp4.compiler.normalizer.NormalizationFactory
implements INormalizationFactory {

	@Override
	public IASTexpression newSuper_with_args(IASTexpression args) {
		return new ASTSuper_with_args(args);
	}
}
