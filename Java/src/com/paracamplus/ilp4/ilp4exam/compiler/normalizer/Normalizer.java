package com.paracamplus.ilp4.ilp4exam.compiler.normalizer;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTSuper_with_args;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTvisitor;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;

public class Normalizer 
extends com.paracamplus.ilp4.compiler.normalizer.Normalizer 
implements 
    IASTvisitor<IASTexpression, INormalizationEnvironment, CompilationException> {

    private INormalizationFactory factory;

	public Normalizer (INormalizationFactory factory,
              IASTCclassDefinition objectClass ) {
		super(factory, objectClass);
		this.factory=factory;
    }

//Méthode a Completer !!!

	@Override
	public IASTexpression visit(IASTSuper_with_args iast, INormalizationEnvironment data)
		throws CompilationException {
		IASTexpression args = iast.getArgs().accept(this, data);
		return ((INormalizationFactory) factory).newSuper_with_args(args);
	}
}
