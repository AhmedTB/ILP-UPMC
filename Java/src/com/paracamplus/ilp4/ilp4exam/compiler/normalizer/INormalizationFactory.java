package com.paracamplus.ilp4.ilp4exam.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTexpression;

public interface INormalizationFactory 
 	extends com.paracamplus.ilp4.compiler.normalizer.INormalizationFactory {
	IASTexpression newSuper_with_args(IASTexpression args);
}
