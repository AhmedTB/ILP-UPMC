package com.paracamplus.ilp4.ilp4exam.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTSuper_with_args;

public class FreeVariableCollector extends com.paracamplus.ilp4.compiler.FreeVariableCollector
implements IASTvisitor<Void, Set<IASTClocalVariable>, CompilationException> {

    public FreeVariableCollector(IASTCprogram program) {
        super(program);
    }

	@Override
	public Void visit(IASTSuper_with_args iast, Set<IASTClocalVariable> data)
		throws CompilationException {
		iast.getArgs().accept(this, data);
		return null;
	}
}
