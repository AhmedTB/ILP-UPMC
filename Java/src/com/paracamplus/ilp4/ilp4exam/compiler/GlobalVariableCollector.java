package com.paracamplus.ilp4.ilp4exam.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTSuper_with_args;

public class GlobalVariableCollector extends com.paracamplus.ilp4.compiler.GlobalVariableCollector
implements IASTvisitor<Set<IASTCglobalVariable>, 
                        Set<IASTCglobalVariable>, 
                        CompilationException> {

	@Override
	public Set<IASTCglobalVariable> visit(IASTSuper_with_args iast, Set<IASTCglobalVariable> data)
		throws CompilationException {
		result = iast.getArgs().accept(this, data);
		return result;
	}
}
