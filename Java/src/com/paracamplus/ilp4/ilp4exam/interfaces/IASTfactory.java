package com.paracamplus.ilp4.ilp4exam.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;

public interface IASTfactory extends com.paracamplus.ilp4.interfaces.IASTfactory {
	IASTexpression newSuper_with_args(IASTexpression args);
}
