package com.paracamplus.ilp4.ilp4exam.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;

public interface IASTSuper_with_args extends IASTexpression {
    
    IASTexpression getArgs();
	
}
