package com.paracamplus.ilp4.ilp4exam.interfaces;

public interface IASTvisitor <Result, Data, Anomaly extends Throwable>
extends com.paracamplus.ilp4.interfaces.IASTvisitor<Result, Data, Anomaly> {
	Result visit(IASTSuper_with_args iast, Data data) throws Anomaly;
}
