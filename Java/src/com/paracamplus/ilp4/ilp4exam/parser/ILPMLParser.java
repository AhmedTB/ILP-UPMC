package com.paracamplus.ilp4.ilp4exam.parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.paracamplus.ilp1.parser.ParseException;
import com.paracamplus.ilp4.interfaces.IASTprogram;

import com.paracamplus.ilp4.ilp4exam.interfaces.IASTfactory;
import com.paracamplus.ilp4.ilp4exam.parser.ILPMLListener;
import antlr4.ILPMLgrammarExamenParser;
import antlr4.ILPMLgrammarExamenLexer;


public class ILPMLParser extends com.paracamplus.ilp4.parser.ilpml.ILPMLParser{
	public ILPMLParser(IASTfactory factory) {
		super(factory);
	}


	public IASTprogram getProgram() throws ParseException {
		try {
			ANTLRInputStream in = new ANTLRInputStream(input.getText());
			// flux de caractères -> analyseur lexical
			ILPMLgrammarExamenLexer lexer = new ILPMLgrammarExamenLexer(in);
			// analyseur lexical -> flux de tokens
			CommonTokenStream tokens =	new CommonTokenStream(lexer);
			// flux tokens -> analyseur syntaxique
			ILPMLgrammarExamenParser parser =	new ILPMLgrammarExamenParser(tokens);
			// démarage de l'analyse syntaxique
			ILPMLgrammarExamenParser.ProgContext tree = parser.prog();		
			// parcours de l'arbre syntaxique et appels du Listener
			ParseTreeWalker walker = new ParseTreeWalker();
			ILPMLListener extractor = new ILPMLListener((IASTfactory)factory);
			walker.walk(extractor, tree);	
			return tree.node;
			} catch (Exception e) {
				throw new ParseException(e);
			}
	}

}
