package com.paracamplus.ilp4.ilp4exam.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;

public class ASTfactory extends com.paracamplus.ilp4.ast.ASTfactory
	implements com.paracamplus.ilp4.ilp4exam.interfaces.IASTfactory {	

	@Override
	public IASTexpression newSuper_with_args(IASTexpression args) {
		return new ASTSuper_with_args(args);
	}
}
