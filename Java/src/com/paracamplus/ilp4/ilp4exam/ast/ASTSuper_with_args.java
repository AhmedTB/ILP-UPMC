package com.paracamplus.ilp4.ilp4exam.ast;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTSuper_with_args;
import com.paracamplus.ilp4.ilp4exam.interfaces.IASTvisitor;
import com.paracamplus.ilp1.ast.ASTexpression;

public class ASTSuper_with_args extends ASTexpression 
implements IASTSuper_with_args {

    public ASTSuper_with_args(IASTexpression[] args) {
        this.args = args;
		
    }
    
    private final IASTexpression[] args;
	
    @Override
	public IASTexpression[] getArgs(){
		return args;
	}
	
    @Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor,
			Data data) throws Anomaly {
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}
