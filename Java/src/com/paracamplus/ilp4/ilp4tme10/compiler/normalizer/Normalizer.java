/* *****************************************************************
 * ilp4 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp4
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.ilp4tme10.compiler.normalizer;

import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTdefined;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTexists;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTvisitor;

public class Normalizer 
extends com.paracamplus.ilp4.compiler.normalizer.Normalizer 
implements 
 IASTvisitor<IASTexpression, INormalizationEnvironment, CompilationException> {


    public Normalizer (INormalizationFactory factory, IASTCclassDefinition objectClass ) {
    	super(factory, objectClass);
    }

	@Override
	public IASTexpression visit(IASTexists iast, INormalizationEnvironment data)
			throws CompilationException {
		IASTvariable variable = iast.getVariable();
		return ((INormalizationFactory) factory).newExists(variable);
	}

	@Override
	public IASTexpression visit(IASTdefined iast, INormalizationEnvironment data)
			throws CompilationException {
		IASTvariable variable = iast.getVariable();
		return ((INormalizationFactory) factory).newDefined(variable);
	}
}
