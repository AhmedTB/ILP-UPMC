/* *****************************************************************
 * ilp4 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp4
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.ilp4tme10.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface INormalizationFactory 
extends com.paracamplus.ilp4.compiler.normalizer.INormalizationFactory {
	
    IASTexpression newExists(IASTvariable variable);
    IASTexpression newDefined(IASTvariable variable);
    
}
