package com.paracamplus.ilp4.ilp4tme10.interfaces;

import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTdefined {
	public IASTvariable getVariable();
}
