/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.ilp4tme10.interpreter;


import java.util.Set;

import com.paracamplus.ilp3.interpreter.primitive.Throw.ThrownException;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTdefined;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTexists;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTvisitor;
import com.paracamplus.ilp4.interfaces.IASTclassDefinition;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.interpreter.interfaces.IClassEnvironment;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;

public class Interpreter extends com.paracamplus.ilp4.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException> {
    
	protected GlobalVariableCollectorInterpreter globals = new GlobalVariableCollectorInterpreter();
	protected Set<String> nomVar;
	
    public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
                        IOperatorEnvironment operatorEnvironment,
                        IClassEnvironment classEnvironment) {
    	super(globalVariableEnvironment, operatorEnvironment, classEnvironment);
    	this.classEnvironment = classEnvironment;
    }

    public Object visit(IASTprogram iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {
        for ( IASTclassDefinition cd : iast.getClassDefinitions() ) {
            this.visit(cd, lexenv);
        }
        for ( IASTfunctionDefinition fd : iast.getFunctionDefinitions() ) {
            Object f = this.visit(fd, lexenv);
            String v = fd.getName();
            getGlobalVariableEnvironment().addGlobalVariableValue(v, f);
        }
        try {
        	
        	globals.visit(iast);
        	nomVar = globals.getGlobals();
        	
            return iast.getBody().accept(this, lexenv);
        } catch (ThrownException exc) {
            return exc.getThrownValue();
        } catch (Exception exc) {
            return exc;
        }
    }
    
    // Ajout TME 10

	@Override
	public Object visit(IASTexists iast, ILexicalEnvironment data)
			throws EvaluationException {
		IASTvariable variable = iast.getVariable();
		String nomV = variable.getMangledName();
		
		// Vérifie dans l'environnement lexical si la variable existe
		if (data.isPresent(variable)) 
			return true;
		
		// Pour chacune des variables globales on regarde si elle existe dans la liste
		if (nomVar.contains(nomV))
			return true;
		
		return false;

	}
	@Override
	public Object visit(IASTdefined iast, ILexicalEnvironment data)
			throws EvaluationException {
		IASTvariable variable = iast.getVariable();
		
		if (data.isPresent(variable)) 
			return true;
		
		return (getGlobalVariableEnvironment().getGlobalVariableValue(variable.getMangledName()) != null);
	}
    
  
}
