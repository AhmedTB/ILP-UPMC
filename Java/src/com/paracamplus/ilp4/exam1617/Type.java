package com.paracamplus.ilp4.exam1617;

public class Type {
	
	private String type;

	public Type(String type){
		this.type=type;
	}
	
	public String getType(){
		return type;
	}
}