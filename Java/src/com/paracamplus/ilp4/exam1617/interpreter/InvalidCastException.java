package com.paracamplus.ilp4.exam1617.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class InvalidCastException extends EvaluationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public InvalidCastException(String msg) {
		super(msg);
		this.msg=msg;
	}
	
	public String getMsg(){
		return msg;
	}
}