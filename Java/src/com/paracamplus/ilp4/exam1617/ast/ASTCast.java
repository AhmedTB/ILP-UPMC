package com.paracamplus.ilp4.exam1617.ast;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.Type;
import com.paracamplus.ilp4.exam1617.interfaces.IASTCast;
import com.paracamplus.ilp4.exam1617.interfaces.IASTvisitor;
import com.paracamplus.ilp1.ast.ASTexpression;

public class ASTCast extends ASTexpression 
implements IASTCast {

    public ASTCast(Type type, IASTexpression val) {
        this.type = type;
		this.val = val;
		
    }
    
    private final Type type;
	private final IASTexpression val;
	
    @Override
	public Type getType(){
		return type;
	}
	@Override
	public IASTexpression getVal(){
		return val;
	}
	
    @Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor,
			Data data) throws Anomaly {
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}
