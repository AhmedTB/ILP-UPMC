package com.paracamplus.ilp4.exam1617.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.Type;

public class ASTfactory extends com.paracamplus.ilp4.ast.ASTfactory
	implements com.paracamplus.ilp4.exam1617.interfaces.IASTfactory {	

	@Override
	public IASTexpression newCast(Type type, IASTexpression val) {
		return new ASTCast(type, val);
	}
}
