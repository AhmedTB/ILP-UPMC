package com.paracamplus.ilp4.exam1617.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.Type;

public interface IASTCast extends IASTexpression {
    
    Type getType();
	IASTexpression getVal();
	
}
