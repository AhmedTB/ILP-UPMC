package com.paracamplus.ilp4.exam1617.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.exam1617.interfaces.IASTvisitor;
import com.paracamplus.ilp4.exam1617.interfaces.IASTCast;

public class FreeVariableCollector extends com.paracamplus.ilp4.compiler.FreeVariableCollector
implements IASTvisitor<Void, Set<IASTClocalVariable>, CompilationException> {

    public FreeVariableCollector(IASTCprogram program) {
        super(program);
    }

	@Override
	public Void visit(IASTCast iast, Set<IASTClocalVariable> data)
		throws CompilationException {
		iast.getVal().accept(this, data);
		return null;
	}
}
