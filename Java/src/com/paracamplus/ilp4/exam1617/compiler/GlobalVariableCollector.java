package com.paracamplus.ilp4.exam1617.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp4.exam1617.interfaces.IASTvisitor;
import com.paracamplus.ilp4.exam1617.interfaces.IASTCast;

public class GlobalVariableCollector extends com.paracamplus.ilp4.compiler.GlobalVariableCollector
implements IASTvisitor<Set<IASTCglobalVariable>, 
                        Set<IASTCglobalVariable>, 
                        CompilationException> {

	@Override
	public Set<IASTCglobalVariable> visit(IASTCast iast, Set<IASTCglobalVariable> data)
		throws CompilationException {
		result = iast.getVal().accept(this, data);
		return result;
	}
}
