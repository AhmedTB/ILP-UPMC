package com.paracamplus.ilp4.exam1617.compiler.normalizer;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.Type;
import com.paracamplus.ilp4.exam1617.ast.ASTCast;

public class NormalizationFactory
extends com.paracamplus.ilp4.compiler.normalizer.NormalizationFactory
implements INormalizationFactory {

	@Override
	public IASTexpression newCast(Type type, IASTexpression val) {
		return new ASTCast(type, val);
	}
}
