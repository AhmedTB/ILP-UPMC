package com.paracamplus.ilp4.exam1617.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.Type;

public interface INormalizationFactory 
 	extends com.paracamplus.ilp4.compiler.normalizer.INormalizationFactory {
	IASTexpression newCast(Type type, IASTexpression val);
}
