package com.paracamplus.ilp4.exam1617.compiler.normalizer;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.exam1617.interfaces.IASTCast;
import com.paracamplus.ilp4.exam1617.interfaces.IASTvisitor;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;

public class Normalizer 
extends com.paracamplus.ilp4.compiler.normalizer.Normalizer 
implements 
    IASTvisitor<IASTexpression, INormalizationEnvironment, CompilationException> {

    private INormalizationFactory factory;

	public Normalizer (INormalizationFactory factory,
              IASTCclassDefinition objectClass ) {
		super(factory, objectClass);
		this.factory=factory;
    }

//Méthode a Completer !!!

	@Override
	public IASTexpression visit(IASTCast iast, INormalizationEnvironment data)
		throws CompilationException {
		IASTexpression val = iast.getVal().accept(this, data);
		return ((INormalizationFactory) factory).newCast(iast.getType(), val);
	}
}
