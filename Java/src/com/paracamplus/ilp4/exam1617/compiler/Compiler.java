package com.paracamplus.ilp4.exam1617.compiler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;

import com.paracamplus.ilp1.compiler.AssignDestination;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.NoDestination;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp1.compiler.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.compiler.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;

import com.paracamplus.ilp4.exam1617.compiler.FreeVariableCollector;
import com.paracamplus.ilp4.exam1617.compiler.GlobalVariableCollector;
import com.paracamplus.ilp4.exam1617.compiler.normalizer.INormalizationFactory;
import com.paracamplus.ilp4.exam1617.compiler.normalizer.NormalizationFactory;
import com.paracamplus.ilp4.exam1617.compiler.normalizer.Normalizer;
import com.paracamplus.ilp4.exam1617.interfaces.IASTvisitor;
import com.paracamplus.ilp4.exam1617.interfaces.IASTCast;


public class Compiler extends com.paracamplus.ilp4.compiler.Compiler 
	implements IASTvisitor<Void, Compiler.Context, CompilationException> {

	public Compiler(IOperatorEnvironment ioe, IGlobalVariableEnvironment igve) {
		super(ioe, igve);
	}
	
    public IASTCprogram normalize(IASTprogram program, 
                                  IASTCclassDefinition objectClass) 
            throws CompilationException {
        INormalizationFactory nf = new NormalizationFactory();
        Normalizer normalizer = new Normalizer(nf, objectClass);
        IASTCprogram newprogram = normalizer.transform(program);
        return newprogram;
    }
   
    public String compile(IASTprogram program, 
                          IASTCclassDefinition objectClass) 
            throws CompilationException {
        
        IASTCprogram newprogram = normalize(program, objectClass);
        newprogram = (IASTCprogram) optimizer.transform(newprogram);

        GlobalVariableCollector gvc = new GlobalVariableCollector();
        Set<IASTCglobalVariable> gvs = gvc.analyze(newprogram);
        newprogram.setGlobalVariables(gvs);
        
        FreeVariableCollector fvc = new FreeVariableCollector(newprogram);
        newprogram = fvc.analyze();
      
        Context context = new Context(NoDestination.NO_DESTINATION);
        StringWriter sw = new StringWriter();
        try {
            out = new BufferedWriter(sw);
            visit(newprogram, context);
            out.flush();
        } catch (IOException exc) {
            throw new CompilationException(exc);
        }
        return sw.toString();
    }

	@Override
	public Void visit(IASTCast iast, Context data) throws CompilationException {
		emit("{\n");
		IASTvariable tmpVal = data.newTemporaryVariable();
		emit("  ILP_Object " + tmpVal.getMangledName() + "; \n");
		Context cVal = data.redirect(new AssignDestination(tmpVal));
		iast.getVal().accept(this, cVal);
		emit(data.destination.compile());
		emit("ILP_cast(");
		emit("\"");	
		emit(iast.getType().getType());
		emit("\",");
		emit(tmpVal.getMangledName());
		emit(");");
		emit("\n}");
		return null;
	}
}
