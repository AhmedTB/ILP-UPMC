/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2016.ilp2.interpreter;

import java.util.List;
import java.util.Vector;

import annale.novembre2016.ilp2.interfaces.IASTfunctionDefinition;
import annale.novembre2016.ilp2.interfaces.IASTinvocation;
import annale.novembre2016.ilp2.interfaces.IASTvisitor;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTboolean;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTfloat;
import com.paracamplus.ilp1.interfaces.IASTinteger;
import com.paracamplus.ilp1.interfaces.IASTstring;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interpreter.EmptyLexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.Invocable;
import com.paracamplus.ilp2.interfaces.IASTprogram;


public class Interpreter extends com.paracamplus.ilp2.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException> {
    

	

	public Object visit(IASTprogram iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {
        for ( com.paracamplus.ilp2.interfaces.IASTfunctionDefinition fd : iast.getFunctionDefinitions() ) {
            Object f = this.visit( (annale.novembre2016.ilp2.interfaces.IASTfunctionDefinition)fd, lexenv);
            String v = fd.getName();
            getGlobalVariableEnvironment().addGlobalVariableValue(v, f);
        }
        try {
            return iast.getBody().accept(this, lexenv);
       } catch (Exception exc) {
            return exc;
        }
    }
	
    // 
    
    public Interpreter(IGlobalVariableEnvironment globalVariableEnvironment,
			IOperatorEnvironment operatorEnvironment) {
		super(globalVariableEnvironment, operatorEnvironment);
	}

   
    // 

    
    public Invocable visit(IASTfunctionDefinition iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {
    	IASTbinding[] bindings = iast.getBindings();
    	
    	ILexicalEnvironment env = new EmptyLexicalEnvironment();
    	
    	for(int i=0 ; i<bindings.length; i++){
    		if(bindings[i].getInitialisation().getClass()== IASTstring.class){
    			env=env.extend(bindings[i].getVariable(),((IASTstring) bindings[i].getInitialisation()).getValue());
    		}
    		else{
    			if(bindings[i].getInitialisation().getClass()!= IASTinteger.class){
    				env=env.extend(bindings[i].getVariable(),((IASTinteger) bindings[i].getInitialisation()).getValue());
    			}
    			else{
    				if(bindings[i].getInitialisation().getClass()!= IASTfloat.class){
    					env=env.extend(bindings[i].getVariable(),((IASTfloat) bindings[i].getInitialisation()).getValue());
    				}
    				else{
    					if(bindings[i].getInitialisation().getClass()!= IASTboolean.class){
    						env=env.extend(bindings[i].getVariable(),((IASTboolean) bindings[i].getInitialisation()).getValue());
    					}
    					else
    						throw new EvaluationException("All variables &keys must have a constant as default value");
    				}
    			}
    		}
    		
    	}

    	
    	IASTvariable[] v = iast.getVariables();
    	
    	IASTvariable[] vars = new IASTvariable[bindings.length];

    	for(int i=0; i<bindings.length; i++)
    		vars[i]=bindings[i].getVariable();
      	
        Invocable fun = new Function(v, vars,
                                     iast.getBody(),
                                     env);
        getGlobalVariableEnvironment()
            .addGlobalVariableValue(iast.getName(), fun);
        return fun;
    }
    
    @Override
	public Object visit(IASTinvocation iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {

        Object function = iast.getFunction().accept(this, lexenv);
        if ( function instanceof Invocable ) {
            Invocable f = (Invocable)function;
            List<Object> args = new Vector<Object>();
            
            IASTbinding[] b = iast.getBindings();
            
            for ( IASTexpression arg : iast.getArguments() ) {
                Object value = arg.accept(this, lexenv);
                args.add(value);
            }
            
            for(int i=0 ; i<b.length; i++){
                Object value = b[i].getInitialisation().accept(this, lexenv);
                args.add(value);
            }
            
            System.out.println("ICI");
            return f.apply(this, args.toArray());
        } else {
            String msg = "Cannot apply " + function;
            throw new EvaluationException(msg);
        }
    }
    



}
