/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2016.ilp2.interfaces;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;

public interface IASTinvocation extends com.paracamplus.ilp1.interfaces.IASTinvocation {
	IASTbinding[] getBindings();
}
