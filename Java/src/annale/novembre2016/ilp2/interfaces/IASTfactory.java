package annale.novembre2016.ilp2.interfaces;

import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;


public interface IASTfactory extends com.paracamplus.ilp2.interfaces.IASTfactory {

	IASTexpression newInvocation(IASTexpression function,
			IASTexpression[] args, IASTblock.IASTbinding[] binds);

	IASTfunctionDefinition newFunctionDefinition(IASTvariable name,
			IASTvariable[] vars, IASTbinding[] bindings,
			IASTexpression body);

}
