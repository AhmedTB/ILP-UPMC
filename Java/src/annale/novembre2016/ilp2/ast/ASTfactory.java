/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2016.ilp2.ast;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;

import annale.novembre2016.ilp2.interfaces.IASTfactory;


public class ASTfactory extends com.paracamplus.ilp2.ast.ASTfactory 
	implements IASTfactory{

	@Override
	public IASTexpression newInvocation(IASTexpression function,
			IASTexpression[] args, IASTbinding[] binds) {
		return new ASTinvocation(function, args, binds);
	}

	@Override
	public IASTfunctionDefinition newFunctionDefinition(IASTvariable functionVariable,
			IASTvariable[] vars, IASTbinding[] bindings, IASTexpression body) {
		return new ASTfunctionDefinition(functionVariable, vars, bindings, body);
	}

    

}
