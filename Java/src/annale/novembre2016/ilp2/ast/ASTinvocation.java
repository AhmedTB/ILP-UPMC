/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2016.ilp2.ast;

import annale.novembre2016.ilp2.interfaces.IASTinvocation;

import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvisitor;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;

public class ASTinvocation extends ASTexpression implements IASTinvocation {
    
    public ASTinvocation (IASTexpression function, IASTexpression[] arguments, IASTbinding[] binds) {
        this.function = function;
        this.arguments = arguments;
        this.binds=binds;
    }
    private final IASTexpression function;
    private final IASTexpression[] arguments;
    private final IASTbinding[] binds;
    
    @Override
	public IASTexpression getFunction () {
        return function;
    }
    @Override
	public IASTexpression[] getArguments () {
        return arguments;
    }

    @Override
	public <Result, Data, Anomaly extends Throwable> 
    Result accept(IASTvisitor<Result, Data, Anomaly> visitor, Data data)
            throws Anomaly {
        return ((annale.novembre2016.ilp2.interfaces.IASTvisitor<Result, Data, Anomaly>)visitor).visit(this, data);
    }
	@Override
	public IASTbinding[] getBindings() {
		return binds;
	}
}
