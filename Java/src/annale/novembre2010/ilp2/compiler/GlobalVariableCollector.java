/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2010.ilp2.compiler;

import java.util.Set;

import annale.novembre2010.ilp2.interfaces.IASTCvisitor;
import annale.novembre2010.ilp2.interfaces.IASTvardef;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;

public class GlobalVariableCollector extends com.paracamplus.ilp2.compiler.GlobalVariableCollector
implements IASTCvisitor<Set<IASTCglobalVariable>, 
                        Set<IASTCglobalVariable>, 
                        CompilationException> {

	@Override
	public Set<IASTCglobalVariable> visit(IASTvardef iast,
			Set<IASTCglobalVariable> data) throws CompilationException {
		result = iast.getVar().accept(this, data);
		if(iast.getBody()!=null)
			result = iast.getBody().accept(this, data);
		return result;
	}


    
	
}
