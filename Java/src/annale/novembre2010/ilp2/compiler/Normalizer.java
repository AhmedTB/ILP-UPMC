/* *****************************************************************
 * ilp2 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp2
 * GPL version 3
 ***************************************************************** */
package annale.novembre2010.ilp2.compiler;


import annale.novembre2010.ilp2.interfaces.IASTvardef;
import annale.novembre2010.ilp2.interfaces.IASTvisitor;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp1.interfaces.IASTexpression;


public class Normalizer 
extends com.paracamplus.ilp2.compiler.normalizer.Normalizer 
implements 
 IASTvisitor<IASTexpression, INormalizationEnvironment, CompilationException> {

    public Normalizer (INormalizationFactory factory) {
    	super(factory);
    }

	@Override
	public IASTexpression visit(IASTvardef iast, INormalizationEnvironment data)
			throws CompilationException {
		
		IASTexpression visitedBody = null;
		
		if(iast.getBody()!=null)
			visitedBody = iast.getBody().accept(this, data);
		
		return ((annale.novembre2010.ilp2.compiler.INormalizationFactory)factory).newVardef(iast.getVar(), visitedBody);
	}



}
