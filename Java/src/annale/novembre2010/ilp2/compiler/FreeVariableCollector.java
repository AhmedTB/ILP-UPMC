/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2010.ilp2.compiler;

import java.util.Set;

import annale.novembre2010.ilp2.interfaces.IASTCvisitor;
import annale.novembre2010.ilp2.interfaces.IASTvardef;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp2.compiler.interfaces.IASTCprogram;


public class FreeVariableCollector extends com.paracamplus.ilp2.compiler.FreeVariableCollector
implements IASTCvisitor<Void, Set<IASTClocalVariable>, CompilationException> {

    public FreeVariableCollector(IASTCprogram program) {
        super(program);
    }

	@Override
	public Void visit(IASTvardef iast, Set<IASTClocalVariable> data)
			throws CompilationException {
		// TODO Auto-generated method stub
		return null;
	}

    

}
