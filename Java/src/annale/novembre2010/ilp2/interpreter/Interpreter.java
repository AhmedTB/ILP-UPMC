/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2010.ilp2.interpreter;

import annale.novembre2010.ilp2.interfaces.IASTvardef;
import annale.novembre2010.ilp2.interfaces.IASTvisitor;

import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;


public class Interpreter extends com.paracamplus.ilp2.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException> {
    

    // 
    
    public Interpreter(IGlobalVariableEnvironment globalVariableEnvironment,
			IOperatorEnvironment operatorEnvironment) {
		super(globalVariableEnvironment, operatorEnvironment);
	}

	@Override
	public Object visit(IASTvardef iast, ILexicalEnvironment data)
			throws EvaluationException {

		 data=data.extend(iast.getVar(), null);
		return (iast.getBody()==null?null : iast.getBody().accept(this, data));
		
	}
	
	 @Override
		public Object visit(IASTvariable iast, ILexicalEnvironment lexenv) 
	            throws EvaluationException {
	        try {

	        	if(lexenv.getValue(iast)==null){
	        		throw new EvaluationException("Error : variable not initialized");
	        	}
	        	else{
	        		return lexenv.getValue(iast);
	        	}
	        } catch (EvaluationException exc) {
	        	
	        	Object res = getGlobalVariableEnvironment()
					    .getGlobalVariableValue(iast.getName());
	        	if(res==null)
	        		throw new EvaluationException("Error : variable not initialized");
	            return res; 
	        }
	    }


}
