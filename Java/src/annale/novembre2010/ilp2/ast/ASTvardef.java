package annale.novembre2010.ilp2.ast;

import com.paracamplus.ilp1.annotation.OrNull;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.IASTvisitor;

import annale.novembre2010.ilp2.interfaces.IASTvardef;

public class ASTvardef extends com.paracamplus.ilp1.ast.ASTexpression implements IASTvardef {
	
	
	private IASTvariable var;
	@OrNull private IASTexpression body;
	
	public ASTvardef(IASTvariable var, IASTexpression body){
		this.var = var;
		this.body = body;
	}
	
	@Override
	public IASTvariable getVar(){
		return var;
	}
	
	@Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			IASTvisitor<Result, Data, Anomaly> visitor, Data data)
			throws Anomaly {
		return ((annale.novembre2010.ilp2.interfaces.IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}

	@Override
	public IASTexpression getBody() {
		return body;
	}

}
