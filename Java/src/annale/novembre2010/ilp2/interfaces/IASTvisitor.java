package annale.novembre2010.ilp2.interfaces;

public interface IASTvisitor<Result, Data, Anomaly extends Throwable> extends com.paracamplus.ilp2.interfaces.IASTvisitor<Result, Data, Anomaly> {
	
	 Result visit(IASTvardef iast, Data data) throws Anomaly;
	
}
