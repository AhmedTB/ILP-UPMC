package annale.novembre2010.ilp2.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp2.interfaces.IASTfactory {

	IASTexpression newVardef(IASTvariable var, IASTexpression body);

}
