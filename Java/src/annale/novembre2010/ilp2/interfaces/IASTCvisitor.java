/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package annale.novembre2010.ilp2.interfaces;




public interface IASTCvisitor<Result, Data, Anomaly extends Throwable> 
extends annale.novembre2010.ilp2.interfaces.IASTvisitor<Result, Data, Anomaly>
 {

}
