package annale.novembre2010.ilp2.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTvardef extends IASTexpression {
	
	public IASTvariable getVar();
	public IASTexpression getBody();

}
