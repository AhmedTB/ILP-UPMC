#include <stdio.h>
#include <stdlib.h>
#include "ilp.h"

/* Global variables */
ILP_Object print;
ILP_Object x;

/* Global prototypes */

/* Global functions */


ILP_Object
ilp_program ()
{
  {
    ILP_Object ilptmp14;
    {
      ILP_Object ilptmp15;
      ilptmp15 = ILP_Integer2ILP (0);
      ilptmp14 = (x = ilptmp15);
    }
    while (1)
      {
	ILP_Object ilptmp16;
	{
	  ILP_Object ilptmp17;
	  ILP_Object ilptmp18;
	  ilptmp17 = x;
	  ilptmp18 = ILP_Integer2ILP (5);
	  ilptmp16 = ILP_LessThan (ilptmp17, ilptmp18);
	}
	if (ILP_isEquivalentToTrue (ilptmp16))
	  {
	    {
	      ILP_Object ilptmp19;
	      {
		ILP_Object ilptmp20;
		{
		  ILP_Object ilptmp21;
		  ILP_Object ilptmp22;
		  ilptmp21 = x;
		  ilptmp22 = ILP_Integer2ILP (2);
		  ilptmp20 = ILP_LessThan (ilptmp21, ilptmp22);
		}
		if (ILP_isEquivalentToTrue (ilptmp20))
		  {
		    {
		      ILP_Object ilptmp23;
		      {
			ILP_Object ilptmp24;
			ILP_Object ilptmp25;
			ilptmp24 = x;
			ilptmp25 = ILP_Integer2ILP (1);
			ilptmp23 = ILP_Plus (ilptmp24, ilptmp25);
		      }
		      ilptmp19 = (x = ilptmp23);
		    }

		  }
		else
		  {
		    {
		      ILP_Object ilptmp26;
		      {
			ILP_Object ilptmp27;
			ilptmp27 = ILP_Integer2ILP (20);
			ilptmp26 = (x = ilptmp27);
		      }
		      continue;
		      ilptmp19 = ilptmp26;
		    }

		  }
	      }
	      {
		ILP_Object ilptmp28;
		ilptmp28 = x;
		ilptmp19 = ILP_print (ilptmp28);
	      }
	      (void) ilptmp19;
	    }

	  }
	else
	  {
	    break;

	  }
      }
    ilptmp14 = ILP_FALSE;
    return ilptmp14;
  }

}

static ILP_Object
ilp_caught_program ()
{
  struct ILP_catcher *current_catcher = ILP_current_catcher;
  struct ILP_catcher new_catcher;

  if (0 == setjmp (new_catcher._jmp_buf))
    {
      ILP_establish_catcher (&new_catcher);
      return ilp_program ();
    };
  return ILP_current_exception;
}

int
main (int argc, char *argv[])
{
  ILP_START_GC;
  ILP_print (ilp_caught_program ());
  ILP_newline ();
  return EXIT_SUCCESS;
}
