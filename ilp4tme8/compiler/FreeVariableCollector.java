package com.paracamplus.ilp4.ilp4tme8.compiler;

import java.util.Set;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;

public class FreeVariableCollector 
extends com.paracamplus.ilp4.compiler.FreeVariableCollector 
implements IASTvisitor<Void, Set<IASTClocalVariable>, CompilationException>{

	public FreeVariableCollector(IASTCprogram program) {
		super(program);
	}

//Methode a coùmpléter
	@Override
	public Void visit(IASTreadProperty iast, Set<IASTClocalVariable> data)
			throws CompilationException {
	    iast.getNom().accept(this, data);
        iast.getProperty().accept(this, data);
        return null;
	}

	@Override
	public Void visit(IASTwriteProperty iast, Set<IASTClocalVariable> data)
			throws CompilationException {
	 	iast.getNom().accept(this, data);
        iast.getProperty().accept(this, data);
        iast.getValue().accept(this, data);
        return null;
	}

	@Override
	public Void visit(IASThasProperty iast, Set<IASTClocalVariable> data)
			throws CompilationException {
		iast.getNom().accept(this, data);
		iast.getProperty().accept(this, data);
        return null;
	}
	
	/**
    	A REDEFINIR EN FONCTION DES CHAMPS DE L'AST
    	
   si l'ast créé est de type break, continue ...
    	 
    	 
	    public Void visit(IASTbreak iast, Set<IASTClocalVariable> variables) throws CompilationException {
	      	return null;
	    }
    
   si l'ast créé est de type while elle contient un body et une condition
    	
	    public Void visit(IASTwhile iast, Set<IASTClocalVariable> variables) throws CompilationException {
	            iast.getCondition().accept(this, variables);
	            iast.getBody().accept(this, variables);
	            return null;
	     }
	 */	
}