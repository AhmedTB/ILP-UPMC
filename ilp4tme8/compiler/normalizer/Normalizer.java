package com.paracamplus.ilp4.ilp4tme8.compiler.normalizer;

import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.normalizer.INormalizationEnvironment;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;


import com.paracamplus.ilp4.ilp4tme8.compiler.normalizer.INormalizationFactory;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;


public class Normalizer 
extends com.paracamplus.ilp4.compiler.normalizer.Normalizer
implements IASTvisitor<IASTexpression, INormalizationEnvironment,
						CompilationException>{
	
	private INormalizationFactory factory;

    public Normalizer (INormalizationFactory factory,
                       IASTCclassDefinition objectClass ) {
    	super(factory, objectClass);
    	this.factory=factory;
    }
	
//Méthode a Completer !!!
	@Override
	public IASTexpression visit(IASTreadProperty iast, INormalizationEnvironment data)
			throws CompilationException {
		Object nom = iast.getNom().accept(this, data);
        Object property = iast.getProperty().accept(this, data);
        return this.factory.newReadProperty((IASTexpression)nom, (IASTexpression)property);
	}

	@Override
	public IASTexpression visit(IASTwriteProperty iast,
			INormalizationEnvironment data) throws CompilationException {
		Object nom = iast.getNom().accept(this, data);
        Object property = iast.getProperty().accept(this, data);
        Object value = iast.getValue().accept(this, data);
        return this.factory.newWriteProperty((IASTexpression)nom, (IASTexpression)property, (IASTexpression)value);
	}

	@Override
	public IASTexpression visit(IASThasProperty iast,
			INormalizationEnvironment data) throws CompilationException {
		Object nom = iast.getNom().accept(this, data);
        Object property = iast.getProperty().accept(this, data);
        return this.factory.newHasProperty((IASTexpression)nom, (IASTexpression)property);
	}	
}