package com.paracamplus.ilp4.ilp4tme8.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4tme8.ast.ASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.ast.ASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.ast.ASTwriteProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;

public class NormalizationFactory 
extends com.paracamplus.ilp4.compiler.normalizer.NormalizationFactory 
implements INormalizationFactory{

	@Override
	public IASTreadProperty newReadProperty(IASTexpression nom,
			IASTexpression property) {
		return new ASTreadProperty(nom, property);
	}

	@Override
	public IASTwriteProperty newWriteProperty(IASTexpression nom,
			IASTexpression property, IASTexpression value) {
		return new ASTwriteProperty(nom, property, value);
	}

	@Override
	public IASThasProperty newHasProperty(IASTexpression nom,
			IASTexpression property) {
		return new ASThasProperty(nom, property);
	}
}