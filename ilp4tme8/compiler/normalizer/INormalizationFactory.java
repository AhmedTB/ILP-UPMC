package com.paracamplus.ilp4.ilp4tme8.compiler.normalizer;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;

public interface INormalizationFactory 
extends com.paracamplus.ilp4.compiler.normalizer.INormalizationFactory{
	
	IASTreadProperty newReadProperty(IASTexpression nom, IASTexpression property);
	IASTwriteProperty newWriteProperty(IASTexpression nom, IASTexpression property, IASTexpression value);
	IASThasProperty	newHasProperty(IASTexpression nom, IASTexpression property);
}