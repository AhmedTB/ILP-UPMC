package com.paracamplus.ilp4.ilp4tme8.compiler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;

import com.paracamplus.ilp1.compiler.AssignDestination;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.NoDestination;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp1.compiler.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.compiler.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;

import com.paracamplus.ilp4.ilp4tme8.compiler.normalizer.INormalizationFactory;
import com.paracamplus.ilp4.ilp4tme8.compiler.normalizer.NormalizationFactory;
import com.paracamplus.ilp4.ilp4tme8.compiler.normalizer.Normalizer;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;
import com.paracamplus.ilp4.ilp4tme8.compiler.FreeVariableCollector;
import com.paracamplus.ilp4.ilp4tme8.compiler.GlobalVariableCollector;


public class Compiler extends com.paracamplus.ilp4.compiler.Compiler 
implements IASTvisitor<Void, Compiler.Context, CompilationException>{

	public Compiler(IOperatorEnvironment ioe, IGlobalVariableEnvironment igve) {
		super(ioe, igve);
	}

	
//SI PAS DE TEMPS FAIRE SCHEMA DE COMPILATION 
//SINON IMPLEMENTER CETTE METHODE EN FONCTION DE L'AST CRÉÉ
	@Override
	public Void visit(IASTreadProperty iast, Context data)
			throws CompilationException {
		emit("{\n");
		IASTvariable tmpNom = data.newTemporaryVariable();
		IASTvariable tmpProperty = data.newTemporaryVariable();
		emit("  ILP_Object " + tmpNom.getMangledName() + "; \n");
		emit("  ILP_Object " + tmpProperty.getMangledName() + "; \n");
		Context cNom = data.redirect(new AssignDestination(tmpNom));
		Context cProperty = data.redirect(new AssignDestination(tmpProperty));
		iast.getNom().accept(this, cNom);
		iast.getProperty().accept(this, cProperty);
		emit(data.destination.compile());
		emit("ILP_read_property(");
		emit(tmpNom.getMangledName());
		emit(",");
		emit(tmpProperty.getMangledName());
		emit(");\n}");
		return null;
	}
	

	@Override
	public Void visit(IASTwriteProperty iast, Context data)
			throws CompilationException {
		emit("{\n");
		IASTvariable tmpNom = data.newTemporaryVariable();
		IASTvariable tmpProperty = data.newTemporaryVariable();
		IASTvariable tmpValue = data.newTemporaryVariable();
		emit("  ILP_Object " + tmpNom.getMangledName() + "; \n");
		emit("  ILP_Object " + tmpProperty.getMangledName() + "; \n");
		emit("  ILP_Object " + tmpValue.getMangledName() + "; \n");
		Context cNom = data.redirect(new AssignDestination(tmpNom));
		Context cProperty = data.redirect(new AssignDestination(tmpProperty));
		Context cValue = data.redirect(new AssignDestination(tmpValue));
		iast.getNom().accept(this, cNom);
		iast.getProperty().accept(this, cProperty);
		iast.getValue().accept(this, cValue);
		emit(data.destination.compile());
		emit("ILP_write_property(");
		emit(tmpNom.getMangledName());
		emit(",");
		emit(tmpProperty.getMangledName());
		emit(",");
		emit(tmpValue.getMangledName());
		emit(");\n}");
		return null;
	}


	@Override
	public Void visit(IASThasProperty iast, Context data)
			throws CompilationException {
		emit("{\n");
		IASTvariable tmpNom = data.newTemporaryVariable();
		IASTvariable tmpProperty = data.newTemporaryVariable();
		emit("  ILP_Object " + tmpNom.getMangledName() + "; \n");
		emit("  ILP_Object " + tmpProperty.getMangledName() + "; \n");
		Context cNom = data.redirect(new AssignDestination(tmpNom));
		Context cProperty = data.redirect(new AssignDestination(tmpProperty));
		iast.getNom().accept(this, cNom);
		iast.getProperty().accept(this, cProperty);
		emit(data.destination.compile());
		emit("ILP_read_property(");
		emit(tmpNom.getMangledName());
		emit(",");
		emit(tmpProperty.getMangledName());
		emit(");\n}");
		return null;
	}

	
    public IASTCprogram normalize(IASTprogram program, 
            IASTCclassDefinition objectClass) 
			throws CompilationException {
		INormalizationFactory nf = new NormalizationFactory();
		Normalizer normalizer = new Normalizer(nf, objectClass);
		IASTCprogram newprogram = normalizer.transform(program);
		return newprogram;
	}
		
	public String compile(IASTprogram program, 
		    IASTCclassDefinition objectClass) 
		throws CompilationException {
		
			IASTCprogram newprogram = normalize(program, objectClass);
			newprogram = (IASTCprogram) optimizer.transform(newprogram);
			
			GlobalVariableCollector gvc = new GlobalVariableCollector();
			Set<IASTCglobalVariable> gvs = gvc.analyze(newprogram);
			newprogram.setGlobalVariables(gvs);
			
			FreeVariableCollector fvc = new FreeVariableCollector(newprogram);
			newprogram = fvc.analyze();
			
			Context context = new Context(NoDestination.NO_DESTINATION);
			StringWriter sw = new StringWriter();
			try {
				out = new BufferedWriter(sw);
				visit(newprogram, context);
				out.flush();
			} catch (IOException exc) {
				throw new CompilationException(exc);
			}
		return sw.toString();
	}
}