package com.paracamplus.ilp4.ilp4tme8.interpreter;

import java.util.List;
import java.util.Vector;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp4.interfaces.IASTinstantiation;
import com.paracamplus.ilp4.interpreter.interfaces.IClass;
import com.paracamplus.ilp4.interpreter.interfaces.IClassEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.ILexicalEnvironment;
import com.paracamplus.ilp1.interpreter.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASThasProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;
import com.paracamplus.ilp4.ilp4tme8.interpreter.ILP9Instance;

public class Interpreter extends com.paracamplus.ilp4.interpreter.Interpreter
implements IASTvisitor<Object, ILexicalEnvironment, EvaluationException>{
	
    protected IClassEnvironment classEnvironment;
	
	public Interpreter (IGlobalVariableEnvironment globalVariableEnvironment,
            IOperatorEnvironment operatorEnvironment,
            IClassEnvironment classEnvironment ) {
		super(globalVariableEnvironment, operatorEnvironment, classEnvironment );
		this.classEnvironment = classEnvironment;
	}
    
    public IClassEnvironment getClassEnvironment () {
        return classEnvironment;
    }

//METHODE A COMPLETER !!!!
	@Override
	public Object visit(IASTreadProperty iast, ILexicalEnvironment data)
			throws EvaluationException {
		
		Object nom = iast.getNom().accept(this, data);
		Object property = iast.getProperty().accept(this, data);

		/* Le nom est objet de type ILP9Instance */
		if(nom instanceof ILP9Instance){
			ILP9Instance instance =(ILP9Instance)nom;
			
			/* property est la propriete qui est une String */
			if(property instanceof String){
				String prop=(String)property;
			
				/* Si la propriete existe on renvoie sa valeur sinon ca leve une exception */
				return instance.readProperty(prop);
			}
			throw new ExceptionProperty(property.toString() + " n'est pas une String !");
		}
		
		throw new ExceptionProperty(nom.toString() + " n'est pas un ILP9Instance !");
	}

	@Override
	public Object visit(IASTwriteProperty iast, ILexicalEnvironment data)
			throws EvaluationException {
		
		Object nom = iast.getNom().accept(this, data);
		Object property = iast.getProperty().accept(this, data);
		Object value = iast.getValue().accept(this, data);
		/* Le nom est objet de type ILP9Instance */
		if(nom instanceof ILP9Instance){
			ILP9Instance instance =(ILP9Instance)nom;
			
			/* property est la propriete qui est une String */
			if(property instanceof String){
				String prop=(String)property;
				
				/* Si la propriete existe on change sa valeursin on ca leve une 
				 exception et on renvoie la valeur precedente de cette propriete */
				return instance.writeProperty(prop, value);
			}
			throw new ExceptionProperty(property.toString() + " n'est pas une String !");
		}
		
		throw new ExceptionProperty(nom.toString() + " n'est pas un ILP9Instance !");
	}

	@Override
	public Object visit(IASThasProperty iast, ILexicalEnvironment data)
			throws EvaluationException {
		Object nom = iast.getNom().accept(this, data);
		Object property = iast.getProperty().accept(this, data);

		/* Le nom est objet de type ILP9Instance */
		if(nom instanceof ILP9Instance){
			ILP9Instance instance =(ILP9Instance)nom;
			
			/* property est la propriete qui est une String */
			if(property instanceof String){
				String prop=(String)property;
			
				/* Si la propriete existe on renvoie TRUE sinon FALSE */
				return instance.hasProperty(prop);
			}
			throw new ExceptionProperty(property.toString() + " n'est pas une String !");
		}
		
		throw new ExceptionProperty(nom.toString() + " n'est pas un ILP9Instance !");
	}
	
	

    @Override
	public Object visit(IASTinstantiation iast, ILexicalEnvironment lexenv) 
            throws EvaluationException {
        IClass clazz = getClassEnvironment().getILP9Class(iast.getClassName());
        List<Object> args = new Vector<Object>();
        for ( IASTexpression arg : iast.getArguments() ) {
            Object value = arg.accept(this, lexenv);
            args.add(value);
        }
        return new ILP9Instance(clazz, args.toArray());
    }  
}