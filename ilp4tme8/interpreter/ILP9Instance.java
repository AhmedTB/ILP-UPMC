package com.paracamplus.ilp4.ilp4tme8.interpreter;

import java.util.HashMap;
import java.util.Map;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp4.interpreter.interfaces.IClass;

public class ILP9Instance extends com.paracamplus.ilp4.interpreter.ILP9Instance {

	private Map<String, Object> propertie=new HashMap<>();
		
	public ILP9Instance(IClass clazz, Object[] fields)
			throws EvaluationException {
		super(clazz, fields);
	}
	
	public Map<String, Object> getProperties(){
		return propertie;
	}
	
	public void addProperty(String property, Object value){
		propertie.put(property, value);
	}
	
	public boolean hasProperty(String property){
		if(propertie.get(property)!=null){
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	public Object readProperty(String property) throws ExceptionProperty{
		if(hasProperty(property)){
			return propertie.get(property);
		}
		throw new ExceptionProperty("La propriété " + property.toString() + " n'existe pas !");
	}
	
	public Object writeProperty(String property, Object value) throws ExceptionProperty{
			Object ValPrecedant=propertie.get(property);
			propertie.put(property, value);
			return ValPrecedant;
	}
}