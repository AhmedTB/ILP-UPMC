package com.paracamplus.ilp4.ilp4tme8.interpreter;

import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class ExceptionProperty extends EvaluationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msg;
	
	public ExceptionProperty(String msg) {
		super(msg);
		this.msg=msg;
	}
	
	public String getMsg() {
		return msg;
	}
}
