package com.paracamplus.ilp4.ilp4tme8.ast;


import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTreadProperty;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;

public class ASTreadProperty extends ASTexpression implements IASTreadProperty{

	private IASTexpression nom;
	private IASTexpression property;
	
	public ASTreadProperty(IASTexpression nom, IASTexpression property) {
		this.nom=nom;
		this.property=property;
	}
	
	@Override
	public IASTexpression getNom() {
		return nom;
	}

	@Override
	public IASTexpression getProperty() {
		return property;
	}
	
	@Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor, 
			Data data) throws Anomaly {
		
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}