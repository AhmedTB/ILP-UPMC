package com.paracamplus.ilp4.ilp4tme8.ast;

import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTvisitor;
import com.paracamplus.ilp4.ilp4tme8.interfaces.IASTwriteProperty;

public class ASTwriteProperty extends ASTexpression implements IASTwriteProperty{
	
	private IASTexpression nom;
	private IASTexpression property;
	private IASTexpression value;
	
	public ASTwriteProperty(IASTexpression nom, IASTexpression property, IASTexpression value) {
		this.nom=nom;
		this.property=property;
		this.value=value;
	}
	
	@Override
	public IASTexpression getNom() {
		return nom;
	}

	@Override
	public IASTexpression getProperty() {
		return property;
	}
	
	@Override
	public IASTexpression getValue() {
		return value;
	}
	
	@Override
	public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor, 
			Data data) throws Anomaly {
		
		return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
	}
}