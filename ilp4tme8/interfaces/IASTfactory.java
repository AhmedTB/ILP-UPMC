package com.paracamplus.ilp4.ilp4tme8.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;


public interface IASTfactory extends com.paracamplus.ilp4.interfaces.IASTfactory{
	
	//herite de l'ancienne IASTFactory
	
	IASTreadProperty newReadProperty(IASTexpression nom, IASTexpression property);
	IASTwriteProperty newWriteProperty(IASTexpression nom, IASTexpression property, IASTexpression value);
	IASThasProperty	newHasProperty(IASTexpression nom, IASTexpression property);

}