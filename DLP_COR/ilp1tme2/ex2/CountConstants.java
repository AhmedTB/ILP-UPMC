package com.paracamplus.ilp1.ilp1tme2.ex2;

import com.paracamplus.ilp1.interfaces.IASTalternative;
import com.paracamplus.ilp1.interfaces.IASTbinaryOperation;
import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTboolean;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTfloat;
import com.paracamplus.ilp1.interfaces.IASTinteger;
import com.paracamplus.ilp1.interfaces.IASTinvocation;
import com.paracamplus.ilp1.interfaces.IASToperator;
import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.interfaces.IASTsequence;
import com.paracamplus.ilp1.interfaces.IASTstring;
import com.paracamplus.ilp1.interfaces.IASTunaryOperation;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.IASTvisitor;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class CountConstants 
implements IASTvisitor<Integer,Void, EvaluationException> {
	    
	    public CountConstants () {
	    	constnum = 0;
	    }
	    protected int constnum;

	    public int getConstNum() {
	        return constnum;
	    }
	    
	    public void incConstNum() {
	         constnum = constnum + 1;
	    }
	    
	    // 
	    
	    public  Integer visit (IASTprogram iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0 ;
	        cpt = iast.getBody().accept(this, unused);
	            return cpt;
	    }
	   
	          
	    @Override
		public Integer visit(IASTalternative iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0;
	        cpt = cpt + iast.getCondition().accept(this, unused);
	        cpt = cpt + iast.getConsequence().accept(this, unused);
	        if ( iast.isTernary() ) {
	        	cpt = cpt + iast.getAlternant().accept(this, unused); 
	        }
	        return cpt;
	    }
	    


	    @Override
		public Integer visit(IASTunaryOperation iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0;
	        cpt = cpt + iast.getOperand().accept(this, unused);
	        return cpt;
	    }
	    
	    @Override
		public Integer visit(IASTbinaryOperation iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0;
	        cpt = cpt + iast.getLeftOperand().accept(this, unused);
	        cpt = cpt + iast.getRightOperand().accept(this, unused);
	        return cpt;
	    }

	    @Override
		public Integer visit(IASToperator iast, Void unused) 
	            throws EvaluationException {
	    	return 0;
	    }

	    @Override
		public Integer visit(IASTsequence iast, Void unused) 
	            throws EvaluationException {
	        IASTexpression[] expressions = iast.getExpressions();
	        int cpt = 0;
	        for ( IASTexpression e : expressions ) {
	        	cpt = cpt +  e.accept(this, unused);
	        }
	        return cpt;
	    }
	    
	    @Override
		public Integer visit(IASTblock iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0;
	        for ( IASTbinding binding : iast.getBindings() ) {
	            cpt =cpt + binding.getInitialisation().accept(this, unused);
	        }
	        return cpt + iast.getBody().accept(this, unused);
	    }

	    @Override
		public Integer visit(IASTboolean iast, Void unused) 
	            throws EvaluationException {
	        return 1;
	    }
	    
	    @Override
		public Integer visit(IASTinteger iast, Void unused) 
	            throws EvaluationException {
	        return 1;
	    }
	    
	    @Override
		public Integer visit(IASTfloat iast, Void unused) 
	            throws EvaluationException {
	        return 1;
	    }
	    
	    @Override
		public Integer visit(IASTstring iast, Void unused) 
	            throws EvaluationException {
	        return 1;
	    }

	    @Override
		public Integer visit(IASTvariable iast, Void unused) 
	            throws EvaluationException {
	       return 0;
	    }
	    
	    @Override
		public Integer visit(IASTinvocation iast, Void unused) 
	            throws EvaluationException {
	    	int cpt = 0;
	        for ( IASTexpression arg : iast.getArguments() ) {
	        	cpt = cpt +  arg.accept(this, unused);
	            }
	            return cpt;
	        }
	    }
	    

