package com.paracamplus.ilp1.ilp1tme2.ex2;

import antlr4.ILPMLgrammar1Parser.ConstFalseContext;
import antlr4.ILPMLgrammar1Parser.ConstFloatContext;
import antlr4.ILPMLgrammar1Parser.ConstIntegerContext;
import antlr4.ILPMLgrammar1Parser.ConstStringContext;
import antlr4.ILPMLgrammar1Parser.ConstTrueContext;

import com.paracamplus.ilp1.interfaces.IASTfactory;

public class ILPMLListener extends com.paracamplus.ilp1.parser.ilpml.ILPMLListener {
	
	private int count;
	
	public ILPMLListener(IASTfactory factory) {
		super(factory);
	}

	public int getCount() {
		return count;
	}

	@Override 
	public void exitConstFloat(
			ConstFloatContext ctx) { 
		super.exitConstFloat(ctx);
		count++;
	}

	@Override 
	public void	exitConstInteger(
			ConstIntegerContext ctx) { 
		super.exitConstInteger(ctx);
		count++;
	}

	@Override 
	public void exitConstFalse(
			ConstFalseContext ctx) { 
		super.exitConstFalse(ctx);
		count++;
	}

	@Override 
	public void exitConstTrue(
			ConstTrueContext ctx) {
		super.exitConstTrue(ctx);
		count++;
	}

	@Override 
	public void exitConstString(
			ConstStringContext ctx) { 
		super.exitConstString(ctx);
		count++;
	}

}
