package com.paracamplus.ilp1.ilp1tme2.ex2.xml;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import com.paracamplus.ilp1.ast.ASTfactory;
import com.paracamplus.ilp1.ilp1tme2.ex2.CountConstants;
import com.paracamplus.ilp1.interfaces.IASTfactory;
import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.test.InterpreterRunner;
import com.paracamplus.ilp1.interpreter.test.InterpreterTest;
import com.paracamplus.ilp1.parser.ParseException;

public class CountingTest extends InterpreterTest
implements ICountingConstantsProcess {

	private XMLParser xmlparser;
	
	public CountingTest(File file) {
		super(file);
	}
	
	@Override
    public void configureRunner(InterpreterRunner run) throws EvaluationException {
		super.configureRunner(run);
		IASTfactory factory =  new ASTfactory();
        this.xmlparser = new XMLParser(factory);
        xmlparser.setGrammar(new File(XMLgrammarFile));
        run.setXMLParser(xmlparser);
	}
	    
	@Override
	public int getNbConstantesDOM() {
		return (xmlparser.getNbConstantes());
	}


	@Override
	public int getNbConstantesAST() throws EvaluationException {
		try {
			IASTprogram program = xmlparser.getProgram();
			CountConstants counter = new CountConstants();
			int value = counter.visit(program, null);
			return value;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}
	  
	@Test
	    public void processFile() throws  ParseException, IOException, EvaluationException {
			InterpreterRunner run = new InterpreterRunner();
			configureRunner(run);
			run.testFile(getFile());
			int cstDOM = getNbConstantesDOM();
			int cstAST = getNbConstantesAST() ;
			System.out.println ("Parser counts " + cstDOM + " ctes");
			System.out.println ("AST counts " + cstAST + " ctes");
			assertEquals(cstDOM,  cstAST);
	    }
	        
	    @Parameters(name = "{0}")
	    public static Collection<File[]> data() throws Exception {
	    	return InterpreterRunner.getFileList(samplesDirName, pattern);
	    }    	

}

