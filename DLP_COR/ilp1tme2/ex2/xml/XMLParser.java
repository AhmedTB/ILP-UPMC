package com.paracamplus.ilp1.ilp1tme2.ex2.xml;

import org.w3c.dom.Element;

import com.paracamplus.ilp1.interfaces.IASTfactory;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.parser.ParseException;



public class XMLParser extends com.paracamplus.ilp1.parser.xml.XMLParser  {

	public XMLParser(IASTfactory factory) {
		super(factory);
		nbConstantes = 0;
	}
	
	  private int nbConstantes ;

	  public int getNbConstantes() {
	        return nbConstantes;
	    }

	  public void setNbConstantes(int nbConstantes) {
	        this.nbConstantes = nbConstantes;
	    }
	  
	  @Override
	public IASTexpression integerConstant (Element e) throws ParseException {
		  	this.nbConstantes++ ;
		    final String description = e.getAttribute("value");
		    return getFactory().newIntegerConstant(description);
		}

	    @Override
		public IASTexpression floatConstant (Element e) throws ParseException {
	    	this.nbConstantes++ ;
	        final String description = e.getAttribute("value");
	        return getFactory().newFloatConstant(description);
	    }

	    @Override
		public IASTexpression stringConstant (Element e) throws ParseException {
	    	this.nbConstantes++ ;
	        final String description = e.getTextContent();
	        return getFactory().newStringConstant(description);
	    }

	    @Override
		public IASTexpression booleanConstant (Element e) throws ParseException {
	    	this.nbConstantes++ ;
	        final String description = e.getAttribute("value");
	        return getFactory().newBooleanConstant(description);
	    }
	    
	  
}

       