grammar ILPMLgrammar1tme2;

@header {
    package antlr4;
}

/*
 * Règles sytaxiques.
 * 
 * ANTLR impose que le nom des règles syntaxique comment par 
 * une minuscule.
 * Ces règles ont la forme "BNF".
 * Chaque règle retourne un objet Java représentant un morceau d'AST.
 * La récursivité directe à gauche est autorisée.
 */

// Structure générale d'un programme 
// MODIFIÉ POUR LE TME2
prog returns [com.paracamplus.ilp2.interfaces.IASTprogram node] 
    : (defs+=globalFunDef ';'?)*  (exprs+=expr ';'?) * EOF
    ;

// Fonction globale
// AJOUTÉ POUR LE TME2
globalFunDef returns [com.paracamplus.ilp2.interfaces.IASTfunctionDefinition node]
    : 'function' name=IDENT '(' vars+=IDENT? (',' vars+=IDENT)* ')'
        body=expr
    ;

/*
 * Expressions
 * 
 * Seule la récursivité directe à gauche est autorisée, ce qui nous
 * oblige à fusionner tous les cas dans une unique règle.
 */
expr returns [com.paracamplus.ilp1.interfaces.IASTexpression node]

// séquence d'instructions
    : '(' exprs+=expr (';'? exprs+=expr)* ';'? ')' # Sequence

// invocation
    | fun=expr '(' args+=expr? (',' args+=expr)* ')' # Invocation

// opérations
    | op=('-' | '!') arg=expr # Unary
    | arg1=expr op=('*' | '/' | '%') arg2=expr # Binary
    | arg1=expr op=('+' | '-') arg2=expr # Binary
    | arg1=expr op=('<' | '<=' | '>' | '>=') arg2=expr # Binary
    | arg1=expr op=('==' | '!=') arg2=expr # Binary
    | arg1=expr op='&' arg2=expr # Binary
    | arg1=expr op=('|' | '^') arg2=expr # Binary
       
// constantes
    | 'true' # ConstTrue
    | 'false' # ConstFalse
    | intConst=INT # ConstInteger
    | floatConst=FLOAT # ConstFloat
    | stringConst=STRING # ConstString

// variables
    | var=IDENT # Variable

// déclaration de variable locale
    | 'let' vars+=IDENT '=' vals+=expr ('and' vars+=IDENT '=' vals+=expr)* 
      'in' body=expr # Binding
 
 // alternative (if then else)
    | 'if' condition=expr 'then' consequence=expr 
        ('else' alternant=expr)? # Alternative

// affectation
// AJOUTÉ POUR LE TME2
    | var=IDENT '=' val=expr # VariableAssign

// boucle while
// AJOUTÉ POUR LE TME2
    | 'while' condition=expr 'do' body=expr # Loop        
    ;
    
       
/*
 * Règles lexicales.
 * 
 * ANTLR impose que le nom des règles lexicales commencent par
 *  une majuscule. 
 * Ces règles prennent la forme d'expressions régulières.
 */

// Identificateurs 
IDENT : [a-zA-Z_] [a-zA-Z0-9_]* ;

// Constantes entières
INT : [0-9]+ ;

// Constantes flottantes
FLOAT : [0-9]* '.' [0-9]* ;

// Constantes chaînes de caractères
STRING : '"' (ESC | ~["\\])*  '"';
ESC : '\\' [\\nrt"];

// Commentaires
LINE_COMMENT : '//' (~[\r\n])* -> skip;
COMMENT : '/*' ('*' ~[/] | ~[*])* '*/' -> skip;

// Espaces
SPACE : [ \t\r\n]+ -> skip;
 