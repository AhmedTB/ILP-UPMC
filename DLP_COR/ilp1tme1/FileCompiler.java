package com.paracamplus.ilp1.ilp1tme1;

import java.io.File;

import com.paracamplus.ilp1.compiler.test.CompilerRunner;
import com.paracamplus.ilp1.compiler.test.CompilerTest;

public class FileCompiler extends CompilerTest {
	
	public FileCompiler(File file) {
		super(file);
	}

	public static void main (String[] argument) {
		if ( argument.length < 1 ) {
			String msg = "Missing ILP filename!";
			throw new RuntimeException(msg);
		}
		File f = new File(argument[0]);
		if ( f.exists() && f.canRead() ) {
			FileCompiler tc = new FileCompiler(f);
			try {
				/*
				 *  code similaire à processFile, mais sans l'appel à checkPrintingAndResult
				 *   ceci afin d'éviter une exception si le .print et le .output sont absents
				 */	        	  
				CompilerRunner run = new CompilerRunner();
				tc.configureRunner(run);
				System.out.println(run.compileAndRun(f));	
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
}
