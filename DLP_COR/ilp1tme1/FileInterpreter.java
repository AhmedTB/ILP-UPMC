package com.paracamplus.ilp1.ilp1tme1;


import java.io.File;

import com.paracamplus.ilp1.interpreter.test.InterpreterRunner;
import com.paracamplus.ilp1.interpreter.test.InterpreterTest;

public class FileInterpreter extends InterpreterTest {

	public FileInterpreter(File file) {
		super(file);
	}	
 
	public static void main (String[] argument) {
		if ( argument.length < 1 ) {
			String msg = "Missing ILP filename!";
			throw new RuntimeException(msg);
		}
		File f = new File(argument[0]);
		if ( f.exists() && f.canRead() ) {
			FileInterpreter tc = new FileInterpreter(f);
			try {
				/*
				 *  code similaire à processFile, mais sans l'appel à checkPrintingAndResult
				 *   ceci afin d'éviter une exception si le .print et le .output sont absents
				 */	        	  
				InterpreterRunner run = new InterpreterRunner();
				tc.configureRunner(run);
				run.testFile(tc.file);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	
}
