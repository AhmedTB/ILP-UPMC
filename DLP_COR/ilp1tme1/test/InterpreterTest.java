package com.paracamplus.ilp1.ilp1tme1.test;

import java.io.File;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.paracamplus.ilp1.interpreter.test.InterpreterRunner;


@RunWith(Parameterized.class)
public class InterpreterTest  extends com.paracamplus.ilp1.interpreter.test.InterpreterTest {
    
    public InterpreterTest(File file) {
		super(file);
	}

	protected static String[] samplesDirName = { "SamplesTME1" }; 
    
    @Parameters(name = "{0}")
    public static Collection<File[]> data() throws Exception {
    	return InterpreterRunner.getFileList(samplesDirName, pattern);
    }    	
    
}
