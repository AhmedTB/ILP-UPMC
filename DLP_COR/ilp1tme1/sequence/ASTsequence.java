package com.paracamplus.ilp1.ilp1tme1.sequence;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;


public class ASTsequence extends com.paracamplus.ilp1.ast.ASTsequence
implements IASTsequence {
  
    
    public ASTsequence(IASTexpression[] expressions) {
		super(expressions);
	}

	@Override
	public IASTexpression[] getAllButLastInstructions() throws EvaluationException {
		IASTexpression[] exprs = this.getExpressions();
        int length = exprs.length;
        try {
        	IASTexpression[] _instructions = new IASTexpression[length-1];
            for (int i = 0; i<length-1; i++) {
                _instructions[i] = exprs[i];
            }
            return _instructions;
        } catch (Exception e) {
            throw new EvaluationException(e.getMessage());
        }
    }
}
