/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.ilp1tme1.sequence;



import java.io.File;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.paracamplus.ilp1.interfaces.IASTfactory;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.test.InterpreterRunner;
import com.paracamplus.ilp1.parser.ilpml.ILPMLParser;
import com.paracamplus.ilp1.parser.xml.IXMLParser;
import com.paracamplus.ilp1.parser.xml.XMLParser;


@RunWith(Parameterized.class)
public class InterpreterTest extends com.paracamplus.ilp1.interpreter.test.InterpreterTest{
    
  public InterpreterTest(final File file) {
        super(file);
  }
  
  @Override public void configureRunner(InterpreterRunner run) throws EvaluationException {
	  // ancienne configuration
	  super.configureRunner(run);
	  
	  // modifications pour utiliser la nouvelle Factory
	  IASTfactory factory = new ASTfactory();
      IXMLParser xmlparser = new XMLParser(factory);
      xmlparser.setGrammar(new File(XMLgrammarFile));
      run.setXMLParser(xmlparser);
      run.setILPMLParser(new ILPMLParser(factory));
	}
   
}
