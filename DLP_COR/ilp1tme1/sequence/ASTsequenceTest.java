package com.paracamplus.ilp1.ilp1tme1.sequence;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.paracamplus.ilp1.interfaces.IASTfactory;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.parser.ParseException;
import com.paracamplus.ilp1.parser.xml.XMLParser;


public class ASTsequenceTest {


    /* Utilitaire pour convertir une chaine XML en AST. */
    public static IASTprogram toAST(XMLParser xMLParser, String xml) throws SAXException, IOException {
    	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			StringReader sr = new StringReader(xml);
            InputSource is = new InputSource(sr);
            Document d = db.parse(is);
            IASTprogram ast;
			try {
				ast = xMLParser.parse(d);
	            return ast;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return null;
            
    }
    
   
    private  XMLParser xmlparser;
    

    @Before
    public  void avantTest()  {
    	IASTfactory factory = new ASTfactory();// to do  new ASTSequence
        this.xmlparser = new com.paracamplus.ilp1.parser.xml.XMLParser(factory);
    }
    
    @After
    public void apresTest()  {
    	System.out.println ("======OK==========");
    }
    
    
    /* Test sur une sequence de 2 termes */
    @Test
    public void testSequence () throws Exception {
        String program = "<sequence>"
              + "<boolean value='true'/>"
              + "<integer value='1'/>"
              + "</sequence>";
        IASTprogram a = toAST(xmlparser, program);
        assertTrue(a.getBody() instanceof IASTsequence);
        IASTsequence iast = (IASTsequence) a.getBody();
        IASTexpression[] iastlist = iast.getExpressions();
        assertNotNull(iastlist);
        assertTrue(iastlist.length == 2);
        IASTexpression[] iastbl = iast.getAllButLastInstructions();
        assertEquals(1, iastbl.length);
    }

    /* Test sur une sequence de 3 termes pour verifier que les termes 
     * restant sont bien dans le bon ordre. */
    @Test
    public void testSequenceThree () throws Exception {
        String program = "<sequence>"
              + "<boolean value='true'/>"
              + "<integer value='1'/>"
              + "<integer value='11'/>"
              + "</sequence>";
        IASTprogram a = toAST(xmlparser, program);
        assertTrue(a.getBody() instanceof IASTsequence);
        IASTsequence iast = (IASTsequence) a.getBody();
        IASTexpression[] iastlist = iast.getExpressions();
        assertNotNull(iastlist);
        assertTrue(iastlist.length == 3);
        IASTexpression[] iastbl = iast.getAllButLastInstructions();
        assertEquals(2, iastbl.length);
    }
    
    /* Test sur une sequence d'un terme seulement */
    @Test
    public void testSequenceOne () throws Exception {
        String program = "<sequence>"
            + "<integer value='1'/>"
            + "</sequence>";
        IASTprogram a = toAST(xmlparser, program);
        System.out.println(a);
        assertTrue(a.getBody() instanceof IASTsequence);
        IASTsequence iast = (IASTsequence) a.getBody();
        IASTexpression[] iastlist = iast.getExpressions();
        assertNotNull(iastlist);
        assertTrue(iastlist.length == 1);
        IASTexpression[] iastbl = iast.getAllButLastInstructions();
        assertEquals(0, iastbl.length);
    }

    /* Test sur une sequence vide: exception attendue! */
    @Test
    public void no_testSequenceEmpty () throws Exception {
        String program = "<sequence>"
            + "</sequence>";
        IASTprogram a = toAST(xmlparser, program);
        assertTrue(a.getBody() instanceof IASTsequence);
        IASTsequence iast = (IASTsequence) a.getBody();
        IASTexpression[] iastlist = iast.getExpressions();
        assertNotNull(iastlist);
        assertTrue(iastlist.length == 0);
        // Les tests supplementaires:
        try {
        	iast.getAllButLastInstructions();
        } catch (Exception e) {
            fail("Pas de getAllButLastInstructions(sequence vide)!");       }
    }
}
