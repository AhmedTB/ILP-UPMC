/*
 * Correction du TME 10 : mots clés.
 * Année 2015-2016
 *
 * Antoine Miné
 */

package com.paracamplus.ilp4.ilp4tme10.interfaces;

/*
 * Interface de visiteur étendu à nos nœuds.
 */

public interface IASTvisitor <Result, Data, Anomaly extends Throwable> 
	extends com.paracamplus.ilp4.interfaces.IASTvisitor<Result,Data,Anomaly> {	
	Result visit(IASTexists iast, Data data) throws Anomaly;
	Result visit(IASTdefined iast, Data data) throws Anomaly;	
}
