/*
 * Correction du TME 10 : mots clés.
 * Année 2015-2016
 *
 * Antoine Miné
 */

package com.paracamplus.ilp4.ilp4tme10.parser.xml;

import org.w3c.dom.Element;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.parser.ParseException;
import com.paracamplus.ilp4.ilp4tme10.interfaces.IASTfactory;

/*
 * Parseur XML enrichi avec nos nouveaux nœuds.
 */

public class XMLParser 
extends com.paracamplus.ilp4.parser.xml.XMLParser {

	// il nous faut une fabrique enrichie, comprenant nos nouveaux noeuds
	protected IASTfactory factoryTME10;
	
	public XMLParser(IASTfactory factory) {
		super(factory);
        addMethod("exists", XMLParser.class);
        addMethod("defined", XMLParser.class);
		factoryTME10 = factory;
	}
	
	public IASTfactory getFactoryTME10() {
		return factoryTME10;
	}
	
    public IASTexpression exists (Element e) throws ParseException {
    	IASTvariable var = getFactoryTME10().newVariable(e.getAttribute("name"));
    	return getFactoryTME10().newExists(var);
    }
	
    public IASTexpression defined (Element e) throws ParseException {
    	IASTvariable var = getFactoryTME10().newVariable(e.getAttribute("name"));
    	return getFactoryTME10().newDefined(var);
    }

}
