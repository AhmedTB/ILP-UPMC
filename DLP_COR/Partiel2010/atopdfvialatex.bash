#!/bin/bash
author='Philippe Wang <Philippe.Wang@lip6.fr>'

coding=utf-8
encoding=utf8
coding=iso-latin-1
encoding=latin1
target=$1
shift

cat > $target.tex <<EOF
% -*- coding: ${coding} ; TeX-PDF-mode:t -*-
\documentclass[12pt,a4paper]{article}
\usepackage[${encoding}]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french,english]{babel}
\usepackage{charter}
\usepackage{a4wide}
\usepackage{listings,xcolor}
\renewcommand*\ttdefault{txtt} % police tt assez compacte et lisible!
\lstset{
basicstyle=\ttfamily\small,
inputencoding=${encoding},
upquote=false,
language=Java,
keywordstyle=\bfseries\color{red!50!black}, 
commentstyle=\color{red!30!blue},
stringstyle=\ttfamily,
showstringspaces=false,
backgroundcolor=\color{black!3!white},
numbers=left,
numberstyle=\color{black}\tiny,
stepnumber=1,
numbersep=5pt,
extendedchars=true,
}
\RequirePackage{geometry}
\geometry{reset,
  papersize={210mm,297mm}, 
  text={190mm,265mm},
  headheight=50pt,
  headsep=10pt,
  hoffset=0cm,
  voffset=0cm,
}
\usepackage{lastpage,url,fancyhdr}
\fancypagestyle{plain}{
   \renewcommand{\headrulewidth}{1pt}
   \renewcommand{\footrulewidth}{1pt}
   \chead{$target -- MI016 ILP 2010oct}
   \lhead{}
   \rhead{}
   \lfoot{}
   \cfoot{page \thepage{}\ sur{}\ \pageref{LastPage}}
   \rfoot{}
 }
\pagestyle{plain}
\usepackage{pgfpages}
\pgfpagesuselayout{2 on 1}[a4paper,landscape,border shrink=1mm]

\begin{document}
EOF
for i in "$@"
do
    md5="$target-$(md5sum $i|head -c 32)"
    cat "$i" > "$md5"
    cat >> "$target.tex" <<EOF

{\color{red!80!blue}\large\bf\verb+$i $(wc -l -c< ${i})+}\\
\begin{lstlisting}
EOF
    iconv -c -f utf-8 -t iso-8859-1 "${md5}" | sed -e 's/\t/  /g' >> "$target.tex"
    cat >> "$target.tex" <<EOF
\end{lstlisting}
EOF
done
cat >> "$target.tex"  <<EOF
\end{document}
EOF
while true ; do
    pdflatex -draftmode "$target.tex" || break
    pdflatex "$target.tex"
    break
done

