package com.paracamplus.ilp4.ilp4tme9.compiler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;

import com.paracamplus.ilp1.compiler.AssignDestination;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.NoDestination;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp1.compiler.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.compiler.interfaces.IOperatorEnvironment;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.Inamed;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.interfaces.IASTsend;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;



import com.paracamplus.ilp4.compiler.normalizer.INormalizationFactory;
import com.paracamplus.ilp4.compiler.normalizer.NormalizationFactory;
import com.paracamplus.ilp4.compiler.normalizer.Normalizer;
import com.paracamplus.ilp4.interfaces.IASTvisitor;
import com.paracamplus.ilp4.compiler.FreeVariableCollector;
import com.paracamplus.ilp4.compiler.GlobalVariableCollector;



public class Compiler extends com.paracamplus.ilp4.compiler.Compiler 
implements IASTvisitor<Void, Compiler.Context, CompilationException>{

	public Compiler(IOperatorEnvironment ioe, IGlobalVariableEnvironment igve) {
		super(ioe, igve);
	}

	 @Override
		public Void visit(IASTsend iast, Context context)
	            throws CompilationException {
	        emit("{ \n");
	        IASTvariable tmpMethod = context.newTemporaryVariable();
	        emit("  ILP_general_function " + tmpMethod.getMangledName() + "; \n");
	        IASTvariable tmpReceiver = context.newTemporaryVariable();
	        emit("  ILP_Object " + tmpReceiver.getMangledName() + "; \n");
	        Context c = context.redirect(new AssignDestination(tmpReceiver));

	        IASTexpression[] arguments = iast.getArguments();
	        IASTvariable[] tmps = new IASTvariable[arguments.length];
	        for ( int i=0 ; i<arguments.length ; i++ ) {
	            IASTvariable tmp = context.newTemporaryVariable();
	            emit("  ILP_Object " + tmp.getMangledName() + "; \n");
	            tmps[i] = tmp;
	        }
	        
	        iast.getReceiver().accept(this, c);
	        for ( int i=0 ; i<arguments.length ; i++ ) {
	            IASTexpression expression = arguments[i];
	            IASTvariable tmp = tmps[i];
	            Context c2 = context.redirect(new AssignDestination(tmp));
	            expression.accept(this, c2);
	        }

	        emit(tmpMethod.getMangledName());
	        emit(" = ILP_find_method_global_cache(");
	        emit(tmpReceiver.getMangledName());
	        emit(", &ILP_object_");
	        emit(Inamed.computeMangledName(iast.getMethodName()));
	        emit("_method, ");
	        emit(1 + arguments.length);
	        emit(");\n");

	        emit(context.destination.compile());
	        emit(tmpMethod.getName());
	        emit("(NULL, ");
	        emit(tmpReceiver.getMangledName());
	        for ( int i = 0 ; i<arguments.length ; i++ ) {
	          emit(", ");
	          emit(tmps[i].getMangledName());
	        }
	        emit(");\n}\n");
	        return null;
	    }
	
	
	
	 public IASTCprogram normalize(IASTprogram program, 
	            IASTCclassDefinition objectClass) 
				throws CompilationException {
			INormalizationFactory nf = new NormalizationFactory();
			Normalizer normalizer = new Normalizer(nf, objectClass);
			IASTCprogram newprogram = normalizer.transform(program);
			return newprogram;
		}
			
		public String compile(IASTprogram program, 
			    IASTCclassDefinition objectClass) 
			throws CompilationException {
			
				IASTCprogram newprogram = normalize(program, objectClass);
				newprogram = (IASTCprogram) optimizer.transform(newprogram);
				
				GlobalVariableCollector gvc = new GlobalVariableCollector();
				Set<IASTCglobalVariable> gvs = gvc.analyze(newprogram);
				newprogram.setGlobalVariables(gvs);
				
				FreeVariableCollector fvc = new FreeVariableCollector(newprogram);
				newprogram = fvc.analyze();
				
				Context context = new Context(NoDestination.NO_DESTINATION);
				StringWriter sw = new StringWriter();
				try {
					out = new BufferedWriter(sw);
					visit(newprogram, context);
					out.flush();
				} catch (IOException exc) {
					throw new CompilationException(exc);
				}
			return sw.toString();
		}
}