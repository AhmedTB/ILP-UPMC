/** Predefined classes */

extern struct ILP_Class ILP_object_Object_class;

struct ILP_Class ILP_object_Class_class = {
     &ILP_object_Class_class,
     { { &ILP_object_Object_class,
         "Class",
         1,
         &ILP_object_super_field,
         2,
         { ILP_print,
           ILP_classOf } } }
};

struct ILP_Class ILP_object_Integer_class = {
     &ILP_object_Class_class,
     { { &ILP_object_Object_class,
         "Integer",
         0,
         NULL,
         2,
         { ILP_print,
           ILP_classOf } } }
};

struct ILP_Field ILP_object_super_field = {
     &ILP_object_Field_class,
     { { &ILP_object_Class_class,
         NULL,
         "super",
         0 } }
};

/** Signal an error. */

ILP_Object
ILP_error (char *message)
{
     snprintf(ILP_the_exception._content.asException.message,
              ILP_EXCEPTION_BUFFER_LENGTH,
              "Error: %s\n",
              message);
     fprintf(stderr, "%s", ILP_the_exception._content.asException.message);
     ILP_the_exception._content.asException.culprit[0] = NULL;
     return ILP_throw((ILP_Object) &ILP_the_exception);
}

/** Signal a type error. */

ILP_Object
ILP_domain_error (char *message, ILP_Object o)
{
     snprintf(ILP_the_exception._content.asException.message,
              ILP_EXCEPTION_BUFFER_LENGTH,
              "Domain error: %s\nCulprit: 0x%p\n",
              message, (void*) o);
     fprintf(stderr, "%s", ILP_the_exception._content.asException.message);
     ILP_the_exception._content.asException.culprit[0] = o;
     ILP_the_exception._content.asException.culprit[1] = NULL;
     return ILP_throw((ILP_Object) &ILP_the_exception);


/** Abort abruptly the whole computation. */

ILP_Object
ILP_die (char *message)
{
     fputs(message, stderr);
     fputc('\n', stderr);
     fflush(stderr);
     exit(EXIT_FAILURE);
}


 /** Find the appropriate method */

ILP_general_function
ILP_find_method (ILP_Object receiver,
                 ILP_Method method,
                 int argc)
{
     ILP_Class oclass = receiver->_class;
     if ( ! ILP_is_subclass_of(oclass, 
                               method->_content.asMethod.class_defining) ) {
          /* Signaler une absence de méthode */
          snprintf(ILP_the_exception._content.asException.message,
                   ILP_EXCEPTION_BUFFER_LENGTH,
                   "No such method %s\nCulprit: 0x%p\n",
                   method->_content.asMethod.name, 
                   (void*) receiver);
          /*DEBUG*/
          fprintf(stderr, "%s", ILP_the_exception._content.asException.message);
          ILP_the_exception._content.asException.culprit[0] = receiver;
          ILP_the_exception._content.asException.culprit[1] = 
               (ILP_Object) method;
          ILP_the_exception._content.asException.culprit[2] = NULL;
          ILP_throw((ILP_Object) &ILP_the_exception);
          /* UNREACHED */
          return NULL;          
     };
     if ( argc != method->_content.asMethod.arity ) {
          /* Signaler une erreur d'arité */
          snprintf(ILP_the_exception._content.asException.message,
                   ILP_EXCEPTION_BUFFER_LENGTH,
                   "Method %s arity error: %d instead of %d\nCulprit: 0x%p\n",
                   method->_content.asMethod.name, 
                   argc,
                   method->_content.asMethod.arity,
                   (void*) receiver);
          /*DEBUG*/
          fprintf(stderr, "%s", ILP_the_exception._content.asException.message);
          ILP_the_exception._content.asException.culprit[0] = receiver;
          ILP_the_exception._content.asException.culprit[1] = 
               (ILP_Object) method;
          ILP_the_exception._content.asException.culprit[2] = NULL;
          ILP_throw((ILP_Object) &ILP_the_exception);
          /* UNREACHED */
          return NULL;
     };
     {
          int index = method->_content.asMethod.index;
          return oclass->_content.asClass.method[index];
     }
}


ILP_general_function
ILP_find_method_global_cache (ILP_Object receiver,
                              ILP_Object method,
                              int argc)
{
  static ILP_Object cache_receiver;
  static ILP_Object cache_method;
  static int cargc;
  static ILP_general_function globalfunc;

  if ( receiver == cache_receiver && method == cache_method && argc == cargc) {
    return globalfunc;
  } else {
    globalfunc = ILP_find_method(receiver, method, argc);
    return globalfunc;
  }
 
}
ILP_Object 
ILP_find_and_call_super_method ( 
     ILP_Method current_method, 
     ILP_general_function super_method, 
     ILP_Object arguments[1] ) 
{ 
     /* assert( super_method != NULL ); */ 
     ILP_Object self = arguments[0];
     int arity = current_method->_content.asMethod.arity;
     switch ( arity ) { 
          case 0: { 
               return (*super_method)(NULL, self); 
          } 
          case 1: { 
               return (*super_method)(NULL, self, arguments[1]); 
          } 
          case 2: { 
               return (*super_method)(NULL, self, arguments[1], arguments[2]); 
          } 
          case 3: { 
               return (*super_method)(NULL, self, arguments[1],  
                                            arguments[2],  
                                            arguments[3]); 
          } 
          default: { 
               snprintf(ILP_the_exception._content.asException.message, 
                        ILP_EXCEPTION_BUFFER_LENGTH, 
                        "Cannot invoke supermethod %snCulprit: 0x%pn", 
                        current_method->_content.asMethod.name,  
                        (void*) self ); 
               /*DEBUG*/ 
               fprintf(stderr, "%s", ILP_the_exception._content.asException.message); 
               ILP_the_exception._content.asException.culprit[0] = self; 
               ILP_the_exception._content.asException.culprit[1] =  
                    (ILP_Object) current_method; 
               ILP_the_exception._content.asException.culprit[2] = NULL; 
               ILP_throw((ILP_Object) &ILP_the_exception); 
               /* UNREACHED */ 
               return NULL; 
          } 
     } 
}


 /** Allocate a box with initial value o */

ILP_Object
ILP_make_box (ILP_Object o)
{
     ILP_Object box = ILP_AllocateBox();
     box->_content.asBox.value = o;
     return box;
}

/** Allocators. */

ILP_Object
ILP_malloc (int size, ILP_Class class)
{
     ILP_Object result = ILP_MALLOC(size);
     if ( result == NULL ) {
          return ILP_die("Memory exhaustion");
     };
     result->_class = class;
     return result;
}

ILP_Object
ILP_make_instance (ILP_Class class) 
{
     int size = sizeof(ILP_Class);
     size += sizeof(ILP_Object) * class->_content.asClass.fields_count;
     return ILP_malloc(size, class);
}

ILP_Object
ILP_make_boolean (int b)
{
     if ( b ) {

       return ILP_TRUE;
     } else {
          return ILP_FALSE;
     }
}

ILP_Object
ILP_make_integer (int d)
{
     ILP_Object result = ILP_AllocateInteger();
     result->_content.asInteger = d;
     return result;
}

ILP_Object
ILP_make_string (char *s)
{
     int size = strlen(s);
     ILP_Object result = ILP_AllocateString(size);
     result->_content.asString._size = size;
     memmove(result->_content.asString.asCharacter, s, size);
     return result;
}

 /** String primitives */

static ILP_Object
ILP_concatenate_strings (ILP_Object o1, ILP_Object o2)
{
     int size1 = o1->_content.asString._size;
     int total_size = size1 + o2->_content.asString._size;
     ILP_Object result = ILP_AllocateString(total_size);
     memmove(&(result->_content.asString.asCharacter[0]), 
             o1->_content.asString.asCharacter,
             o1->_content.asString._size);
     memmove(&(result->_content.asString.asCharacter[size1]),
             o2->_content.asString.asCharacter,
             o2->_content.asString._size);
     return result;
}

/** Unary operators */
ILP_Object
ILP_make_negation (ILP_Object o)
{
     ILP_CheckIfBoolean(o);
     {
          if ( ILP_isTrue(o) ) {
               return ILP_FALSE;
          } else {
               return ILP_TRUE;
          }
     }
}

 /** Binary operators */

/* DefineOperator(addition, +) is incorrect since + may (as in javascript)
 * concatenate strings. */

ILP_Object
ILP_make_addition (ILP_Object o1, ILP_Object o2)
{
     if ( ILP_isInteger(o1) ) {
          if ( ILP_isInteger(o2) ) {
               ILP_Object result = ILP_AllocateInteger();
               result->_content.asInteger =
                    o1->_content.asInteger + o2->_content.asInteger;
               return result;
          } else if ( ILP_isFloat(o2) ) {
               ILP_Object result = ILP_AllocateFloat();
               result->_content.asFloat =
                    o1->_content.asInteger + o2->_content.asFloat;
               return result;
          } else {
               return ILP_domain_error("Not a number", o2);
          }
     } else if ( ILP_isFloat(o1) ) {
          if ( ILP_isInteger(o2) ) {
               ILP_Object result = ILP_AllocateFloat();
               result->_content.asFloat =
                    o1->_content.asFloat + o2->_content.asInteger;
               return result;
          } else if ( ILP_isFloat(o2) ) {
               ILP_Object result = ILP_AllocateFloat();
               result->_content.asFloat =
                    o1->_content.asFloat + o2->_content.asFloat;
               return result;
          } else {
               return ILP_domain_error("Not a number", o2);
          }
     } else if ( ILP_isString(o1) ) {
          if ( ILP_isString(o2) ) {
               return ILP_concatenate_strings(o1, o2);
          } else {
               return ILP_domain_error("Not a string", o2);
          }
     } else {
          return ILP_domain_error("Not addable", o1);
     }
}

 DefineOperator(subtraction, -)
DefineOperator(multiplication, *)
DefineOperator(division, /)

/* DefineOperator(modulo, %) is incorrect since modulo works only on
 * integers. */

ILP_Object
ILP_make_modulo (ILP_Object o1, ILP_Object o2)
{
     if ( ILP_isInteger(o1) ) {
          if ( ILP_isInteger(o2) ) {
               ILP_Object result = ILP_AllocateInteger();
               result->_content.asInteger =
                    o1->_content.asInteger % o2->_content.asInteger;
               return result;
          } else {
               return ILP_domain_error("Not an integer", o2);
          }
     } else {
          return ILP_domain_error("Not an integer", o1);
     }
}

 /** Yields the class of an object. The class is also an object
 * as advocated by ObjVlisp. */

ILP_Object
ILP_classOf (ILP_Object self) 
{
     return (ILP_Object) (self->_class);
}

ILP_Object
ILPm_classOf (ILP_Closure useless, ILP_Object self)
{
     return (ILP_Object) (self->_class);
}

/* end of ilpObj.c */


 /* Ajout Exam1617  */

ILP_Object
ILP_cast(char *newType, ILP_Object receiver){
    
  /* Caster un int */
    if(ILP_isInteger(receiver)){
        int entier = receiver->_content.asInteger;
        
        if(strcmp(newType, "int")==0){
            return receiver;
        }else if(strcmp(newType, "float")==0){
            double d = (double)entier;
            ILP_Object result = ILP_make_float(d);
            return result;
        }else if(strcmp(newType, "string")==0){
            char d[1];
            sprintf(d, "%d", entier); 
            ILP_Object result = ILP_make_string(d);
            return result;
        }else{
            return ILP_die("Pas le bon type pour caster un entier");
        }
        
   /* Caster un float */
    }else if(ILP_isFloat(receiver)){
        double decimal = receiver->_content.asFloat;
         
        if(strcmp(newType, "float")==0){
            return receiver;
        }else if(strcmp(newType, "int")==0){
            int d = (int)decimal;
            ILP_Object result = ILP_make_integer(d);
            return result;
        }else if(strcmp(newType, "string")==0){
            char d[1];
            sprintf(d, "%f", decimal); 
            ILP_Object result = ILP_make_string(d);
            return result;
        }else{
            return ILP_die("Pas le bon type pour caster un float");
        }
    
  /* Caster en boolean */
    }else if(ILP_isBoolean(receiver)){
       
        if(strcmp(newType, "bool")==0){
            return receiver;
        }else if(strcmp(newType, "string")==0){
            if(receiver->_content.asBoolean == 1){
                return ILP_make_string("true");
            }else{
                return ILP_make_string("false");
            }
        }else{
            return ILP_die("Pas le bon type pour caster un boolean");
        }
    
  /*  Caster en String */
    }else if(ILP_isString(receiver)){
        
        if(strcmp(newType, "string")==0){
            return receiver;
        }else{
            receiver->_class = &ILP_object_String_class;
            return receiver;
        }
    
  /*  Caster en Objet */ 
    }else{
      if(strcmp(newType, "int")==0 || strcmp(newType, "bool")==0 || strcmp(newType, "float")==0   ){
               
           return ILP_die("Pas le bon type pour caster un objet");
      }
      
      if(strcmp(newType, "string")==0){
            char buf[1024];
            int i;
            
            strcat(buf, "<");
            strcat(buf,receiver->_class->_content.asClass.name);
            strcat(buf, ":");
            for(i=0; i<receiver->_class->_content.asClass.fields_count; i++){
                strcat(buf,receiver->_content.asInstance.field[i]->_content.asField.name);
                strcat(buf,"=");
          //      strcat(buf,receiver->_content.asInstance.field[i]);
                strcat(buf, ":");
            }
            
            strcat(buf, ">");
      }
    
        ILP_Class oclass = receiver->_class;
        
        if(strcmp(oclass->_content.asClass.name, newType)==0){
            return receiver;
        }
        
          /* Object's superclass is NULL */
        while ( oclass ) {
            if ( strcmp(oclass->_content.asClass.name, newType)==0 ) {
                break;
            }     
            oclass = oclass->_content.asClass.super;
        }
          
          if(oclass == NULL){
            return ILP_die("Cast en un type qui n'est pas une sous classe");
          }
    
        return receiver;
 
    }
}
/* Fin Ajout Exam1617  */
